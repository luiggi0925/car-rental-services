package pe.edu.cibertec.carleasing.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import pe.edu.cibertec.carleasing.domain.Carro;
import pe.edu.cibertec.carleasing.dto.CarroDto;
import pe.edu.cibertec.carleasing.dto.MarcaDto;
import pe.edu.cibertec.carleasing.dto.ModeloDto;
import pe.edu.cibertec.carleasing.main.BaseComponentTest;
import pe.edu.cibertec.carleasing.mapper.CarroMapper;

public class CarroServiceImplTest extends BaseComponentTest {

    @Autowired
    private CarroServiceImpl carroServiceImpl;

    @Autowired
    private CarroMapper carroMapper;

    @Test
    public void save() {
        MarcaDto marcaDto = new MarcaDto();
        marcaDto.setId(1L);
        ModeloDto modeloDto = new ModeloDto();
        modeloDto.setId(1L);

        CarroDto carroDto = new CarroDto();
        carroDto.setMarca(marcaDto);
        carroDto.setModelo(modeloDto);
        carroDto.setAnio(1990);
        carroDto.setDetalle("Muy bonito");
        carroDto.setIdUsuario(1L);
        carroDto.setPasajeros(5);
        //Carro carro = new Carro()

        Carro carro = carroMapper.mapToCarro(carroDto);
        carroServiceImpl.save(carro);
        Carro insertado = carroServiceImpl.findById(carro.getId());
        assertThat(insertado, is(notNullValue()));
        assertThat(insertado.getAnio(), equalTo(1990));
        assertThat(insertado.getModelo().getId(), equalTo(1L));
    }
}
