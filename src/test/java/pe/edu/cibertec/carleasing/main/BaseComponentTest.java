package pe.edu.cibertec.carleasing.main;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
//@ComponentScan("pe.edu.cibertec.carleasing")
//@EnableJpaRepositories("pe.edu.cibertec.carleasing.repository")
//@EntityScan("pe.edu.cibertec.carleasing.model")
//@EnableTransactionManagement
//@Rollback
//@ContextConfiguration
@SpringBootTest(properties="classpath:application.properties", classes=Main.class)
public abstract class BaseComponentTest {

}
