package pe.edu.cibertec.carleasing.domain.search;

public enum OrdenCarro {

    PRECIO_DIARIO_ASC(1),
    PRECIO_DIARIO_DESC(2),
    MARCA_ASC_MODELO_ASC(3),
    MARCA_ASC_MODELO_DESC(4),
    MARCA_DESC_MODELO_ASC(5),
    MARCA_DESC_MODELO_DESC(6),
    ANIO_ASC(7),
    ANIO_DESC(8);

    private final int orden;

    private OrdenCarro(int orden) {
        this.orden = orden;
    }

    public int getOrden() {
        return orden;
    }

    public static OrdenCarro getByOrdenValue(int orden) {
        for (OrdenCarro val : OrdenCarro.values()) {
            if (val.getOrden() == orden) {
                return val;
            }
        }
        return null;
    }
}
