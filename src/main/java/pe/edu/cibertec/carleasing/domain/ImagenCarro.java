package pe.edu.cibertec.carleasing.domain;

import static javax.persistence.FetchType.LAZY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import pe.edu.cibertec.carleasing.domain.base.BaseEntity;

@Entity
@Table(name="imagen_carro")
public class ImagenCarro extends BaseEntity {

    @ManyToOne(fetch=LAZY)
    @JoinColumn(name = "idcarro")
    private Carro carro;

    private String nombre;

    @Column(name="imagen")
    private byte[] imagen;

    public ImagenCarro() {
    }

    public ImagenCarro(Long id, Long carroId) {
        this.id = id;
        this.carro = new Carro();
        carro.setId(carroId);
    }

    public Carro getCarro() {
        return carro;
    }

    public void setCarro(Carro carro) {
        this.carro = carro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }
}
