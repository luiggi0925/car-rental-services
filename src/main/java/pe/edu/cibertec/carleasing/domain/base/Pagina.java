package pe.edu.cibertec.carleasing.domain.base;

import java.util.List;

public final class Pagina<E> {

    private final long totalElementos;
    private final long desde;
    private final long hasta;
    private final List<E> elementos;
    private final long tamanioPagina;
    private final long totalPaginas;
    private final long paginaActual;

    public Pagina(long totalElementos, long desde, long hasta, List<E> elementos) {
        this.totalElementos = totalElementos;
        this.desde = desde;
        this.hasta = hasta;
        this.elementos = elementos;
        this.tamanioPagina = hasta - desde + 1;
        this.totalPaginas = (totalElementos + tamanioPagina - 1) / tamanioPagina;
        this.paginaActual = (desde + tamanioPagina - 1) / tamanioPagina;
    }

    public long getTotalElementos() {
        return totalElementos;
    }

    public long getDesde() {
        return desde;
    }

    public long getHasta() {
        return hasta;
    }

    public List<E> getElementos() {
        return elementos;
    }

    public long getTamanioPagina() {
        return tamanioPagina;
    }

    public long getTotalPaginas() {
        return totalPaginas;
    }

    public long getPaginaActual() {
        return paginaActual;
    }
}
