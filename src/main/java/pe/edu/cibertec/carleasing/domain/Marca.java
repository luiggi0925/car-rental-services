package pe.edu.cibertec.carleasing.domain;

import static javax.persistence.FetchType.EAGER;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import pe.edu.cibertec.carleasing.domain.base.BaseEntity;

@Entity
public class Marca extends BaseEntity {

    private String nombre;
    @OneToMany(mappedBy="marca", fetch=EAGER)
    private List<Modelo> listaModelo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Modelo> getListaModelo() {
        return listaModelo;
    }

    public void setListaModelo(List<Modelo> listaModelo) {
        this.listaModelo = listaModelo;
    }
}
