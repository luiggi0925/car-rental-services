package pe.edu.cibertec.carleasing.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import pe.edu.cibertec.carleasing.domain.base.BaseEntity;

@Entity
public class Carro extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "idmodelo")
    private Modelo modelo;
    private int anio;
    private String detalle;
    private int pasajeros;
    @ManyToOne
    @JoinColumn(name = "idusuario")
    private Usuario duenio;
    @OneToMany(mappedBy = "carro", cascade={CascadeType.PERSIST, CascadeType.REMOVE })
    private List<ImagenCarro> listaImagenCarro;
    @Column(name="precioDiario")
    private BigDecimal precioDiario;
    private boolean disponible = true;

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getPasajeros() {
        return pasajeros;
    }

    public void setPasajeros(int pasajeros) {
        this.pasajeros = pasajeros;
    }

    public Usuario getDuenio() {
        return duenio;
    }

    public void setDuenio(Usuario duenio) {
        this.duenio = duenio;
    }

    public List<ImagenCarro> getListaImagenCarro() {
        return listaImagenCarro;
    }

    public void setListaImagenCarro(List<ImagenCarro> listaImagenCarro) {
        this.listaImagenCarro = listaImagenCarro;
    }

    public BigDecimal getPrecioDiario() {
        return precioDiario;
    }

    public void setPrecioDiario(BigDecimal precioDiario) {
        this.precioDiario = precioDiario;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }
}
