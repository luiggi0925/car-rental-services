package pe.edu.cibertec.carleasing.domain.status;

public enum PagoReservaCarro {

    PAGO_EFECTIVO,
    PAGO_TARJETA
}
