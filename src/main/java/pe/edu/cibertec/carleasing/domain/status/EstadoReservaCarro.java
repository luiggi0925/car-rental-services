package pe.edu.cibertec.carleasing.domain.status;

public enum EstadoReservaCarro {

    PENDIENTE,
    EN_CURSO,
    CANCELADO,
    TERMINADA
}
