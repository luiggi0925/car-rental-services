package pe.edu.cibertec.carleasing.domain;

import java.math.BigDecimal;
import java.util.Date;

import pe.edu.cibertec.carleasing.domain.base.BaseEntity;
import pe.edu.cibertec.carleasing.domain.status.EstadoReservaCarro;

public class ReservaCarro extends BaseEntity {

    private Long idCarro;
    private Date fechaInicio;
    private Date fechaFin;
    private Usuario cliente;
    private Integer puntaje;
    private String comentario;
    private BigDecimal precioDiario;
    private BigDecimal precioTotal;
    private EstadoReservaCarro estado;

    public Long getIdCarro() {
        return idCarro;
    }

    public void setIdCarro(Long idCarro) {
        this.idCarro = idCarro;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Usuario getCliente() {
        return cliente;
    }

    public void setCliente(Usuario cliente) {
        this.cliente = cliente;
    }

    public Integer getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(Integer puntaje) {
        this.puntaje = puntaje;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public BigDecimal getPrecioDiario() {
        return precioDiario;
    }

    public void setPrecioDiario(BigDecimal precioDiario) {
        this.precioDiario = precioDiario;
    }

    public BigDecimal getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(BigDecimal precioTotal) {
        this.precioTotal = precioTotal;
    }

    public EstadoReservaCarro getEstado() {
        return estado;
    }

    public void setEstado(EstadoReservaCarro estado) {
        this.estado = estado;
    }
}
