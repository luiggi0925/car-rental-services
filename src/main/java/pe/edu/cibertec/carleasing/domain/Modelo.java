package pe.edu.cibertec.carleasing.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import pe.edu.cibertec.carleasing.domain.base.BaseEntity;

@Entity
public class Modelo extends BaseEntity {

    private String nombre;
    @ManyToOne
    @JoinColumn(name = "idmarca")
    private Marca marca;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }
}
