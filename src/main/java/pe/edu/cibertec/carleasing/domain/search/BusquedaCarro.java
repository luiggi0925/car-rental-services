package pe.edu.cibertec.carleasing.domain.search;

import java.math.BigDecimal;

public class BusquedaCarro {

    private Long idUsuario;
    private Long idMarca;
    private Long idModelo;
    private int anioDesde;
    private String detalle;
    private int pasajeros;
    private BigDecimal precioMinimo;
    private BigDecimal precioMaximo;
    private OrdenCarro ordenCarro = OrdenCarro.PRECIO_DIARIO_ASC;

    private int desde;
    private int hasta;

    public BusquedaCarro() {
        
    }

    public BusquedaCarro(Long idUsuario, Long idMarca, Long idModelo, int anioDesde, String detalle, int pasajeros,
            BigDecimal precioMinimo, BigDecimal precioMaximo, OrdenCarro ordenCarro, int desde, int hasta) {
        super();
        this.idUsuario = idUsuario;
        this.idMarca = idMarca;
        this.idModelo = idModelo;
        this.anioDesde = anioDesde;
        this.detalle = detalle;
        this.pasajeros = pasajeros;
        this.precioMinimo = precioMinimo;
        this.precioMaximo = precioMaximo;
        this.ordenCarro = ordenCarro;
        this.desde = desde;
        this.hasta = hasta;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public Long getIdMarca() {
        return idMarca;
    }

    public Long getIdModelo() {
        return idModelo;
    }

    public int getAnioDesde() {
        return anioDesde;
    }

    public String getDetalle() {
        return detalle;
    }

    public int getPasajeros() {
        return pasajeros;
    }

    public BigDecimal getPrecioMinimo() {
        return precioMinimo;
    }

    public BigDecimal getPrecioMaximo() {
        return precioMaximo;
    }

    public OrdenCarro getOrdenCarro() {
        return ordenCarro;
    }

    public int getDesde() {
        return desde;
    }

    public int getHasta() {
        return hasta;
    }

    @Override
    public String toString() {
        return "ParametrosBusqueda [idUsuario=" + idUsuario + ", descripcion=" + detalle + ", desde=" + desde
                + ", hasta=" + hasta + "]";
    }
}