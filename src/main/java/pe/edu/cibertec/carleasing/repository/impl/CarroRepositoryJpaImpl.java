package pe.edu.cibertec.carleasing.repository.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import pe.edu.cibertec.carleasing.domain.Carro;
import pe.edu.cibertec.carleasing.domain.base.Pagina;
import pe.edu.cibertec.carleasing.domain.search.BusquedaCarro;
import pe.edu.cibertec.carleasing.repository.CarroRepository;
import pe.edu.cibertec.carleasing.repository.base.impl.RepositorioPaginableJpaImpl;

@Repository("carroRepositoryJpa")
public class CarroRepositoryJpaImpl
    extends RepositorioPaginableJpaImpl<Carro, Long>
    implements CarroRepository {

    @Override
    public Pagina<Carro> findByUserId(Long idUsuario, int desde, int hasta) {
        return getPagina("SELECT COUNT(c) FROM Carro c WHERE c.duenio.id = :idUsuario",
                "SELECT c FROM Carro c WHERE c.duenio.id = :idUsuario", new HashMap<String, Object>(){{
                    put("idUsuario", idUsuario);
                }}, desde, hasta);
    }

    @Override
    public Pagina<Carro> findByParameters(BusquedaCarro busquedaCarro) {
        String queryBase = "FROM Carro c WHERE (:idUsuario = 0L OR c.duenio.id = :idUsuario)"
                + " AND (:idMarca = 0L OR c.modelo.marca.id = :idMarca)"
                + " AND (:idModelo = 0L OR c.modelo.id = :idModelo)"
                + " AND (c.anio >= :anioDesde)"
                + " AND (:detalle IS NULL OR c.detalle LIKE :detalle)"
                + " AND (:pasajeros = 0 OR c.pasajeros = :pasajeros)"
                + " AND (c.precioDiario BETWEEN :precioMinimo AND :precioMaximo)"
                + " AND c.disponible = TRUE";
        String countQuery = "SELECT COUNT(c) " + queryBase;
        String query = "SELECT c " + queryBase +" ORDER BY ";
        switch (busquedaCarro.getOrdenCarro()) {
        case PRECIO_DIARIO_DESC:
            query += "c.precioDiario DESC";
        case ANIO_ASC:
            query += "c.anio ASC";
        case ANIO_DESC:
            query += "c.anio DESC";
        case MARCA_ASC_MODELO_ASC:
            query += "c.modelo.marca.id ASC, c.modelo.id ASC";
        case MARCA_ASC_MODELO_DESC:
            query += "c.modelo.marca.id ASC, c.modelo.id DESC";
        case MARCA_DESC_MODELO_ASC:
            query += "c.modelo.marca.id DESC, c.modelo.id ASC";
        case MARCA_DESC_MODELO_DESC:
            query += "c.modelo.marca.id DESC, c.modelo.id DESC";
        case PRECIO_DIARIO_ASC: default:
            query += "c.precioDiario ASC";
        }
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idUsuario", busquedaCarro.getIdUsuario());
        parameters.put("idMarca", busquedaCarro.getIdMarca());
        parameters.put("idModelo", busquedaCarro.getIdModelo());
        parameters.put("anioDesde", busquedaCarro.getAnioDesde());
        parameters.put("detalle", busquedaCarro.getDetalle() == null ? null : "%" + busquedaCarro.getDetalle() + "%");
        parameters.put("pasajeros", busquedaCarro.getPasajeros());
        parameters.put("precioMinimo", busquedaCarro.getPrecioMinimo());
        parameters.put("precioMaximo", busquedaCarro.getPrecioMaximo());
        return getPagina(countQuery, query, parameters, busquedaCarro.getDesde(), busquedaCarro.getHasta());
    }
}
