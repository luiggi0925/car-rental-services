package pe.edu.cibertec.carleasing.repository.base;

import static java.util.Collections.emptyMap;

import java.io.Serializable;
import java.util.Map;

public interface RepositorioPaginable<E, K extends Serializable>
    extends RepositorioBase<E, K>, Paginable<E> {

    long getTotal(String jpql, Map<String, Object> parametros);

    default long getTotal(String jpql) {
        return getTotal(jpql, emptyMap());
    }
}
