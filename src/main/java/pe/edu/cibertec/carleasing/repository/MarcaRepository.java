package pe.edu.cibertec.carleasing.repository;

import pe.edu.cibertec.carleasing.domain.Marca;
import pe.edu.cibertec.carleasing.repository.base.RepositorioBase;

public interface MarcaRepository extends RepositorioBase<Marca, Long> {

    
}
