package pe.edu.cibertec.carleasing.repository.base;

import java.io.Serializable;
import java.util.List;

public interface RepositorioBase<E, K extends Serializable> {

    void guardar(E entity);
    void actualizar(E entity);
    void eliminar(K id);
    E getPorId(K id);
    List<E> getTodos();
}
