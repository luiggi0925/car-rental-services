package pe.edu.cibertec.carleasing.repository.impl;

import org.springframework.stereotype.Repository;

import pe.edu.cibertec.carleasing.domain.Marca;
import pe.edu.cibertec.carleasing.repository.MarcaRepository;
import pe.edu.cibertec.carleasing.repository.base.impl.RepositorioBaseJpaImpl;

@Repository("marcaRepositoryJpa")
public class MarcaRepositoryJpaImpl
    extends RepositorioBaseJpaImpl<Marca, Long>
    implements MarcaRepository {

}
