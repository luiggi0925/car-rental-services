package pe.edu.cibertec.carleasing.repository.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import pe.edu.cibertec.carleasing.domain.ImagenCarro;
import pe.edu.cibertec.carleasing.repository.ImagenCarroRepository;
import pe.edu.cibertec.carleasing.repository.base.impl.RepositorioBaseJpaImpl;

@Repository("imagenCarroRepositoryJpa")
public class ImagenCarroRepositoryJpaImpl
    extends RepositorioBaseJpaImpl<ImagenCarro, Long>
    implements ImagenCarroRepository {

    @Override
    public List<ImagenCarro> findImagenCarroByIdCarro(Long idCarro) {
        return ejecutarQueryComoLista(
                crearQueryTipado(
                    "SELECT new pe.edu.cibertec.carleasing.model.ImagenCarro(ic.id, ic.carro.id) FROM ImagenCarro ic WHERE ic.carro.id = :idCarro",
                    new HashMap<String, Object>() {{
                        put("idCarro", idCarro);
                    }})
                );
    }

    @Override
    public ImagenCarro findByIdAndIdCarro(Long idCarro, Long idImagenCarro) {
        return ejecutarQueryComoResultadoUnico(
                crearQueryTipado(
                    "SELECT ic FROM ImagenCarro ic WHERE ic.id = :idImagenCarro AND ic.carro.id = :idCarro",
                    new HashMap<String, Object>() {{
                        put("idCarro", idCarro);
                        put("idImagenCarro", idImagenCarro);
                    }})
                );
    }

}
