package pe.edu.cibertec.carleasing.repository.base;

import static java.util.Collections.emptyMap;

import java.util.Map;

import pe.edu.cibertec.carleasing.domain.base.Pagina;
import pe.edu.cibertec.carleasing.util.Constantes;

public interface Paginable<E> {

    Pagina<E> getPagina(String countJpql, String jpql, Map<String, Object> parametros, int desde, int hasta);
    default Pagina<E> getPagina(String countJpql, String jpql, Map<String, Object> parametros) {
        return getPagina(countJpql, jpql, parametros, 0, Constantes.TAMANIO_BASE_PAGINA);
    }
    default Pagina<E> getPagina(String countJpql, String jpql) {
        return getPagina(countJpql, jpql, emptyMap());
    }
    Pagina<E> getPagina(int desde, int hasta);
    default Pagina<E> getPagina() {
        return getPagina(0, Constantes.TAMANIO_BASE_PAGINA);
    }
}
