package pe.edu.cibertec.carleasing.repository;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.ImagenCarro;
import pe.edu.cibertec.carleasing.repository.base.RepositorioBase;

public interface ImagenCarroRepository extends RepositorioBase<ImagenCarro, Long> {

    List<ImagenCarro> findImagenCarroByIdCarro(Long idCarro);

    ImagenCarro findByIdAndIdCarro(Long idCarro, Long idImagenCarro);
}
