package pe.edu.cibertec.carleasing.repository;

import pe.edu.cibertec.carleasing.domain.Carro;
import pe.edu.cibertec.carleasing.domain.base.Pagina;
import pe.edu.cibertec.carleasing.domain.search.BusquedaCarro;
import pe.edu.cibertec.carleasing.repository.base.RepositorioPaginable;

public interface CarroRepository extends RepositorioPaginable<Carro, Long> {

    Pagina<Carro> findByUserId(Long idUsuario, int desde, int hasta);
    Pagina<Carro> findByParameters(BusquedaCarro busquedaCarro);
}
