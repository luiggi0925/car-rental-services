package pe.edu.cibertec.carleasing.repository.base.impl;

import static pe.edu.cibertec.carleasing.util.ObjetoUtils.getLista;
import static pe.edu.cibertec.carleasing.util.ObjetoUtils.getMapa;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.core.GenericTypeResolver;

import pe.edu.cibertec.carleasing.repository.base.RepositorioBase;
import pe.edu.cibertec.carleasing.util.Constantes;
import pe.edu.cibertec.carleasing.util.ObjetoUtils;

public abstract class RepositorioBaseJpaImpl<E, K extends Serializable> implements RepositorioBase<E, K> {

    @PersistenceContext
    protected EntityManager em;

    protected Class<E> claseEntidad;

    @SuppressWarnings("unchecked")
    public RepositorioBaseJpaImpl() {
        claseEntidad = (Class<E>) GenericTypeResolver.resolveTypeArguments(getClass(), RepositorioBaseJpaImpl.class)[0];
    }

    @Override
    public void guardar(E entity) {
        em.persist(entity);
    }

    @Override
    public void actualizar(E entity) {
        em.merge(entity);
    }

    @Override
    public void eliminar(K id) {
        E entity = em.find(claseEntidad, id);
        if (entity != null) {
            em.remove(entity);
        }
    }

    @Override
    public E getPorId(K id) {
        return em.find(claseEntidad, id);
    }

    protected TypedQuery<E> crearQueryTipado(String jpql, Map<String, Object> parametros, int desde, int hasta) {
        return crearQuery(jpql, claseEntidad, parametros, desde, hasta);
    }

    protected TypedQuery<E> crearQueryTipado(String jpql, Map<String, Object> parametros) {
        return crearQueryTipado(jpql, parametros, 0, Constantes.TAMANIO_BASE_PAGINA);
    }

    protected TypedQuery<E> crearQuery(String jpql, Class<E> tipo, Map<String, Object> parametros) {
        return crearQuery(jpql, tipo, parametros, 0, Constantes.TAMANIO_BASE_PAGINA);
    }

    protected TypedQuery<E> crearQuery(String jpql, Class<E> tipo, Map<String, Object> parametros, int desde, int hasta) {
        TypedQuery<E> query = em.createQuery(jpql,tipo);
        getMapa(parametros).entrySet().forEach(e -> query.setParameter(e.getKey(), e.getValue()));
        setLimites(query, desde, hasta);
        return query;
    }

    protected void setLimites(TypedQuery<E> query, int desde, int hasta) {
        desde = desde < 0 ? 0 : desde;
        hasta = (hasta < desde) ? desde + Constantes.TAMANIO_BASE_PAGINA : hasta;
        if (desde >= 0 && hasta >= desde) {
            query.setFirstResult(desde);
            query.setMaxResults(hasta - desde);
        }
    }

    protected Query crearQuery(String jpql, Map<String, Object> parametros) {
        return crearQuery(jpql, parametros, 0, Constantes.TAMANIO_BASE_PAGINA);
    }

    protected Query crearQuery(String jpql, Map<String, Object> parametros, int desde, int hasta) {
        Query query = em.createQuery(jpql);
        getMapa(parametros).entrySet().forEach(e -> query.setParameter(e.getKey(), e.getValue()));
        setLimites(query, desde, hasta);
        return query;
    }

    protected void setLimites(Query query, int desde, int hasta) {
        if (desde >= 0 && hasta >= desde) {
            query.setFirstResult(desde);
            query.setMaxResults(hasta - desde);
        }
    }

    protected List<E> ejecutarQueryComoLista(TypedQuery<E> query) {
        return ObjetoUtils.getLista(query.getResultList());
    }

    protected E ejecutarQueryComoResultadoUnico(TypedQuery<E> query) {
        return query.getSingleResult();
    }

    protected CriteriaQuery<E> queryParaTodos() {
        CriteriaBuilder cb = em.getCriteriaBuilder();  
        CriteriaQuery<E> query = cb.createQuery(claseEntidad);
        Root<E> root = query.from(claseEntidad);
        return query.select(root);
    }

    @Override
    public List<E> getTodos() {
        return getLista(em.createQuery(queryParaTodos()).getResultList());
    }
}
