package pe.edu.cibertec.carleasing.repository.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import pe.edu.cibertec.carleasing.domain.Usuario;
import pe.edu.cibertec.carleasing.repository.UsuarioRepository;
import pe.edu.cibertec.carleasing.repository.base.impl.RepositorioPaginableJpaImpl;

@Repository("usuarioRepositoryJpa")
public class UsuarioRepositoryJpaImpl
    extends RepositorioPaginableJpaImpl<Usuario, Long>
    implements UsuarioRepository {

    @Override
    public List<Usuario> findByDni(String dni) {
        return ejecutarQueryComoLista(
                crearQueryTipado(
                "SELECT u FROM Usuario u WHERE dni = :dni", new HashMap<String, Object>(){{
            put("dni", dni);
        }}));
    }

    @Override
    public Usuario findByEmail(String email) {
        return ejecutarQueryComoResultadoUnico(
                crearQueryTipado(
                        "SELECT u FROM Usuario u WHERE email = :email", new HashMap<String, Object>(){{
                    put("email", email);
                        }})
                );
    }
}
