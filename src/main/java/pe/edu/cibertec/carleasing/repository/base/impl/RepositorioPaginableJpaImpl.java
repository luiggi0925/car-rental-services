package pe.edu.cibertec.carleasing.repository.base.impl;

import static pe.edu.cibertec.carleasing.util.ObjetoUtils.getLista;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import pe.edu.cibertec.carleasing.domain.base.Pagina;
import pe.edu.cibertec.carleasing.repository.base.RepositorioPaginable;

public abstract class RepositorioPaginableJpaImpl<E, K extends Serializable>
    extends RepositorioBaseJpaImpl<E, K>
    implements RepositorioPaginable<E, K> {

    @Override
    public Pagina<E> getPagina(String countJpql, String jpql, Map<String, Object> parametros, int desde, int hasta) {
        TypedQuery<E> query = crearQuery(jpql, claseEntidad, parametros, desde, hasta);
        long totalElementos = getTotal(countJpql, parametros);
        return new Pagina<>(totalElementos, desde, hasta, getLista(query.getResultList()));
    }

    @Override
    public long getTotal(String jpql, Map<String, Object> parametros) {
        Query query = crearQuery(jpql, parametros);
        return (Long)query.getSingleResult();
    }

    public long getTotal() {
        CriteriaBuilder cb = em.getCriteriaBuilder();  
        CriteriaQuery<Long> query = cb.createQuery(Long.class);
        Root<E> root = query.from(claseEntidad);
        query.select(cb.count(root));
        return (Long) em.createQuery(query).getSingleResult();
    }

    @Override
    public Pagina<E> getPagina(int desde, int hasta) {
        TypedQuery<E> query = em.createQuery(queryParaTodos());
        setLimites(query, desde, hasta);
        return new Pagina<>(getTotal(), desde, hasta, getLista(query.getResultList()));
    }
}
