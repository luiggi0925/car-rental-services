package pe.edu.cibertec.carleasing.repository;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.Modelo;
import pe.edu.cibertec.carleasing.repository.base.RepositorioBase;

public interface ModeloRepository extends RepositorioBase<Modelo, Long> {

    List<Modelo> findByMarca(Long idMarca);
}
