package pe.edu.cibertec.carleasing.repository;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.Usuario;
import pe.edu.cibertec.carleasing.repository.base.RepositorioPaginable;

public interface UsuarioRepository extends RepositorioPaginable<Usuario, Long> {

    List<Usuario> findByDni(String dni);
    Usuario findByEmail(String email);
}
