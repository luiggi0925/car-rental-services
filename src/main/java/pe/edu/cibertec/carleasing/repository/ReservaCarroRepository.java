package pe.edu.cibertec.carleasing.repository;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.ReservaCarro;
import pe.edu.cibertec.carleasing.repository.base.RepositorioPaginable;

public interface ReservaCarroRepository extends RepositorioPaginable<ReservaCarro, Long> {

    List<ReservaCarro> obtenerPorCarro(Long idCarro);

    List<ReservaCarro> obtenerPorCliente(Long idCliente);

    void actualizarCalificacion(ReservaCarro reservaCarro);

    long obtenerCantidadReservasParaFechas(ReservaCarro reservaCarro);
}
