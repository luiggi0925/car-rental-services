package pe.edu.cibertec.carleasing.repository.impl;

import java.util.HashMap;
import java.util.List;

import pe.edu.cibertec.carleasing.domain.Modelo;
import pe.edu.cibertec.carleasing.repository.ModeloRepository;
import pe.edu.cibertec.carleasing.repository.base.impl.RepositorioBaseJpaImpl;

//@Repository("modeloRepositoryJpa")
public class ModeloRepositoryImpl
    extends RepositorioBaseJpaImpl<Modelo, Long>
    implements ModeloRepository {

    @Override
    public List<Modelo> findByMarca(Long idMarca) {
        return ejecutarQueryComoLista(
                crearQueryTipado("SELECT m FROM Modelo m WHERE m.marca.id = :idMarca", new HashMap<String, Object>() {{
                    put("idMarca", idMarca);
                }}, 0, 0)
                );
    }
}
