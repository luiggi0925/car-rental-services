package pe.edu.cibertec.carleasing.util;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import pe.edu.cibertec.carleasing.domain.base.BaseEntity;
import pe.edu.cibertec.carleasing.domain.base.Pagina;
import pe.edu.cibertec.carleasing.dto.base.BaseDto;

public final class MapperUtils {

    public static <T, E> List<E> mapList(List<T> lista, Function<? super T, ? extends E> convert) {
        return convert == null ? emptyList() : Optional.ofNullable(lista).orElse(emptyList())
                .stream().map(convert).collect(toList());
    }

    public static <T, E> E map(T element, Function<? super T, ? extends E> convert) {
        return Optional.ofNullable(element).map(convert).get();
    }

    public static <T extends BaseEntity, E extends BaseDto> ResponseEntity<?> mapSaveResponse(T mapMe, Function<? super T, ? extends E> convert) {
        return Optional.ofNullable(mapMe)
                .map( toMap -> {
                    E mapped = convert.apply(toMap);
                    URI location = ServletUriComponentsBuilder
                        .fromCurrentRequest().path("/{id}")
                        .buildAndExpand(mapped.getId()).toUri();

                    return ResponseEntity.created(location).build();
                })
                .orElse(ResponseEntity.noContent().build())
                ;
    }

    public static <T, E> ResponseEntity<E> mapResponse(T mapMe, Function<? super T, ? extends E> convert) {
        return Optional.ofNullable(mapMe)
                .map( toMap -> {
                    E mapped = convert.apply(toMap);
                    return ResponseEntity.ok(mapped);
                })
                .orElse(ResponseEntity.notFound().<E>build())
                ;
    }

    public static <E> List<E> asList(Iterable<? extends E> iter) {
        List<E> result;
        if (iter == null) {
            result = emptyList();
        } else {
            result = new LinkedList<>();
            iter.forEach(result::add);
        }
        return result;
    }

    public static <T1, T2> Pagina<T2> mapPagina(Pagina<T1> pagina, Function<? super T1, ? extends T2> convert) {
        return new Pagina<T2>(
                pagina.getTotalElementos(),
                pagina.getDesde(),
                pagina.getHasta(),
                mapList(pagina.getElementos(), convert));
    }
}
