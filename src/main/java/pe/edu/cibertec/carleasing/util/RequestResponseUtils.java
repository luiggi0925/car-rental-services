package pe.edu.cibertec.carleasing.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestResponseUtils {

    private static final Logger logger = LoggerFactory.getLogger(RequestResponseUtils.class);

    public static String getMimeType(String fileName) {
        String mimeType = "application/octet-stream";
        try {
            String fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            switch (fileExtension.toLowerCase()) {
            case "png":
                mimeType = "image/png";
                break;
            case "jpg": case "jpeg":
                mimeType = "image/jpeg";
                break;
            case "gif":
                mimeType = "image/gif";
                break;
            default:
                logger.warn("Mime type couldn't be determined for file '{}'. Using 'application/octet-stream'.", fileName);
            }
        } catch (Exception e) {
            logger.warn(String.format("Mime type couldn't be determined for file '%s'. Using octet-stream", fileName), e);
        }
        return mimeType;
    }

    public static void fileDownload(byte[] data, String fileName, HttpServletResponse response)
            throws IOException {
        try (InputStream is = new ByteArrayInputStream(data)) {
            response.setContentType(getMimeType(fileName));
            response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));

            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        }
    }
}
