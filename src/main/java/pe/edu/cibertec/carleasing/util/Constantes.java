package pe.edu.cibertec.carleasing.util;

public interface Constantes {
    int TAMANIO_BASE_PAGINA = 10;

    static interface URL {
        String FORMATO_URL_USUARIO = "/usuarios/%d";

        String FORMATO_URL_CARROS_USUARIO = "/carros?idUsuario=%d";
        String FORMATO_URL_IMAGEN_CARRO = "/carros/%d/imagenes/%d";
    }
}
