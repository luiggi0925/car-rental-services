package pe.edu.cibertec.carleasing.mapper;

import org.mapstruct.Mapper;

import pe.edu.cibertec.carleasing.domain.Marca;
import pe.edu.cibertec.carleasing.domain.Modelo;
import pe.edu.cibertec.carleasing.dto.MarcaDto;
import pe.edu.cibertec.carleasing.dto.ModeloDto;

@Mapper
public interface MarcaModeloMapper {

    MarcaDto mapToMarcaDto(Marca marca);
    Marca mapToMarca(MarcaDto marcaDto);

    ModeloDto mapToModeloDto(Modelo modelo);
    Modelo mapToModelo(ModeloDto modeloDto);
}
