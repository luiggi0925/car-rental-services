package pe.edu.cibertec.carleasing.mapper;

import java.io.IOException;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.multipart.MultipartFile;

import pe.edu.cibertec.carleasing.controller.CarroController;
import pe.edu.cibertec.carleasing.domain.Carro;
import pe.edu.cibertec.carleasing.domain.ImagenCarro;
import pe.edu.cibertec.carleasing.dto.CarroDto;
import pe.edu.cibertec.carleasing.util.Constantes;

@Mapper(componentModel="spring",
        uses={ MarcaModeloMapper.class })
public interface CarroMapper {

    @Mappings({
        @Mapping(source="duenio.id", target="idUsuario" ),
        @Mapping(source="modelo.marca", target="marca" ),
        @Mapping(source="listaImagenCarro", target="rutasImagenes"),
        @Mapping(source="duenio.id", target="rutaUsuario")
    })
    CarroDto mapToCarroDto(Carro carro);

    @Mappings({
        @Mapping(target="duenio", expression="java( new Usuario(carroDto.getIdUsuario()) )" ),
        @Mapping(target="listaImagenCarro", source="files" )
    })
    Carro mapToCarro(CarroDto carroDto);

    default ImagenCarro mapToImagenCarro(MultipartFile file) {
        ImagenCarro imagenCarro = new ImagenCarro();
        try {
            imagenCarro.setNombre(file.getOriginalFilename());
            imagenCarro.setImagen(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
        return imagenCarro;
    }

    default String mapToRutaImagen(ImagenCarro imagenCarro) {
        if (imagenCarro == null) return null;
        return ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(
                    CarroController.class).descargarImagenCarro(imagenCarro.getCarro().getId(), imagenCarro.getId(), null)
                ).toUri().toString();
    }

    default String mapToRutaUsuario(Integer idUsuario) {
        if (idUsuario == null) return null;
        return String.format(Constantes.URL.FORMATO_URL_USUARIO, idUsuario);
    }
}
