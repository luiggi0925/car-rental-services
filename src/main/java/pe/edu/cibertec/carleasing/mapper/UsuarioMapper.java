package pe.edu.cibertec.carleasing.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import pe.edu.cibertec.carleasing.domain.Usuario;
import pe.edu.cibertec.carleasing.dto.UsuarioDto;
import pe.edu.cibertec.carleasing.util.Constantes;

@Mapper
public interface UsuarioMapper {

    @Mappings(
        @Mapping(target="urlCarros", source="id")
    )
    UsuarioDto mapToUsuarioDto(Usuario usuario);

    Usuario mapToUsuario(UsuarioDto usuarioDto);

    default String mapToRutaCarros(Long idUsuario) {
        if (idUsuario == null) return null;
        return String.format(Constantes.URL.FORMATO_URL_CARROS_USUARIO, idUsuario);
    }
}
