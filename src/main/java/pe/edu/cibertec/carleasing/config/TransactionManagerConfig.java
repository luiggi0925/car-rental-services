package pe.edu.cibertec.carleasing.config;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class TransactionManagerConfig {

    @Bean
    public JpaTransactionManager jpaTransactionManager(EntityManagerFactory emf) {
        JpaTransactionManager tm = new JpaTransactionManager();
        tm.setEntityManagerFactory(emf);
        return tm;
    }

//    @Bean
//    public DataSourceTransactionManager mybatisTransactionManager(DataSource dataSource) {
//        DataSourceTransactionManager dtm = new DataSourceTransactionManager();
//        dtm.setDataSource(dataSource);
//        return dtm;
//    }
}
