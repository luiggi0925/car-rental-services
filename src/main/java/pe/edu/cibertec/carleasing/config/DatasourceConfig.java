package pe.edu.cibertec.carleasing.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatasourceConfig {

    @Value("${dataSource.dataSourceClassName}")
    private String dataSourceClassName;
    @Value("${dataSource.maximumPoolSize}")
    private int maximumPoolSize;
    @Value("${dataSource.idleTimeout}")
    private long idleTimeoutMs;

    @Value("${dataSource.url}")
    private String databaseUrl;
    @Value("${dataSource.user}")
    private String databaseUser;
    @Value("${dataSource.password}")
    private String databasePassword;


    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSourceClassName(dataSourceClassName);
        hikariConfig.setMaximumPoolSize(maximumPoolSize);
        hikariConfig.setIdleTimeout(idleTimeoutMs);
        Properties dsProperties = new Properties();
        dsProperties.setProperty("url", databaseUrl);
        dsProperties.setProperty("user", databaseUser);
        dsProperties.setProperty("password", databasePassword);
        hikariConfig.setDataSourceProperties(dsProperties);
        return hikariConfig;
    }

    @Bean(destroyMethod="close")
    public DataSource dataSource(HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }
}
