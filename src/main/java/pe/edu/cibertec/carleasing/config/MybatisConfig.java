package pe.edu.cibertec.carleasing.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("pe.edu.cibertec.carleasing.repository")
public class MybatisConfig {

    @Autowired
    private ApplicationContext ctx;

    @Bean
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource)
        throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setConfigLocation(ctx.getResource("classpath:configuration/mybatis-configuration.xml"));
        //sessionFactory.setTypeAliasesPackage("pe.edu.cibertec.carleasing.model");
        return sessionFactory.getObject();
    }
}
