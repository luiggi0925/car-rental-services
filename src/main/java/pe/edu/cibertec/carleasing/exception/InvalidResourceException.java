package pe.edu.cibertec.carleasing.exception;

public class InvalidResourceException extends RuntimeException {

    private final Object identifier;
    private final String resourceName;

    public InvalidResourceException(Object identifier, String resourceName) {
        this(identifier, resourceName, createMessage(identifier, resourceName), null);
    }

    public InvalidResourceException(Object identifier, String resourceName, String message) {
        this(identifier, resourceName, message, null);
    }

    public InvalidResourceException(Object identifier, String resourceName, Throwable cause) {
        this(identifier, resourceName, createMessage(identifier, resourceName), cause);
    }

    public InvalidResourceException(Object identifier, String resourceName, String message, Throwable cause) {
        super(message, cause);
        this.identifier = identifier;
        this.resourceName = resourceName;
    }

    public Object getIdentifier() {
        return identifier;
    }

    public String getResourceName() {
        return resourceName;
    }

    private static String createMessage(Object identifier, String resourceName) {
        return String.format("El recurso %s con id %s no existe o no se puede identificar.", identifier.toString(), resourceName);
    }
}
