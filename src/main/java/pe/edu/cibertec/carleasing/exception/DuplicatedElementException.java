package pe.edu.cibertec.carleasing.exception;

public class DuplicatedElementException extends RuntimeException {

    public DuplicatedElementException(String message) {
        super(message);
    }

    public DuplicatedElementException(Throwable cause) {
        super(cause);
    }

    public DuplicatedElementException(String message, Throwable cause) {
        super(message, cause);
    }
}
