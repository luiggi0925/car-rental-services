package pe.edu.cibertec.carleasing.dto;

import pe.edu.cibertec.carleasing.domain.Modelo;
import pe.edu.cibertec.carleasing.dto.base.BaseDto;

public class ModeloDto extends BaseDto {

    private String nombre;

    public ModeloDto() { }

    public ModeloDto(Modelo modelo) {
        this.id = modelo.getId();
        this.nombre = modelo.getNombre();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
