package pe.edu.cibertec.carleasing.dto;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonInclude;

import pe.edu.cibertec.carleasing.dto.base.BaseDto;

@JsonInclude(NON_NULL)
public class CarroDto extends BaseDto {

    private MarcaDto marca;
    private ModeloDto modelo;
    private int anio;
    private String detalle;
    private int pasajeros;
    private Long idUsuario;
    private String rutaUsuario;
    private MultipartFile[] files;
    private List<String> rutasImagenes;
    private BigDecimal precioDiario;

    public MarcaDto getMarca() {
        return marca;
    }

    public void setMarca(MarcaDto marca) {
        this.marca = marca;
    }

    public ModeloDto getModelo() {
        return modelo;
    }

    public void setModelo(ModeloDto modelo) {
        this.modelo = modelo;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getPasajeros() {
        return pasajeros;
    }

    public void setPasajeros(int pasajeros) {
        this.pasajeros = pasajeros;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public MultipartFile[] getFiles() {
        return files;
    }

    public String getRutaUsuario() {
        return rutaUsuario;
    }

    public void setRutaUsuario(String rutaUsuario) {
        this.rutaUsuario = rutaUsuario;
    }

    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }

    public List<String> getRutasImagenes() {
        return rutasImagenes;
    }

    public void setRutasImagenes(List<String> rutasImagenes) {
        this.rutasImagenes = rutasImagenes;
    }

    public BigDecimal getPrecioDiario() {
        return precioDiario;
    }

    public void setPrecioDiario(BigDecimal precioDiario) {
        this.precioDiario = precioDiario;
    }
}
