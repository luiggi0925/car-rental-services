package pe.edu.cibertec.carleasing.dto;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import pe.edu.cibertec.carleasing.dto.base.BaseDto;

@JsonInclude(NON_NULL)
public class UsuarioDto extends BaseDto {

    private String dni;
    private String contrasena;
    private String nombre;
    private String apellido;
    private String email;
    private Date fechaNacimiento;
    private String urlCarros;

    public UsuarioDto() { }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getUrlCarros() {
        return urlCarros;
    }

    public void setUrlCarros(String urlCarros) {
        this.urlCarros = urlCarros;
    }
}
