package pe.edu.cibertec.carleasing.dto;

import pe.edu.cibertec.carleasing.domain.Marca;
import pe.edu.cibertec.carleasing.dto.base.BaseDto;

public class MarcaDto extends BaseDto {

    private String nombre;
//    @JsonProperty("modelos")
    //private List<ModeloDto> listaModelo;

    public MarcaDto() { }

    public MarcaDto(Marca marca) {
        this.id = marca.getId();
        this.nombre = marca.getNombre();
//        this.listaModelo = MapperUtil.mapList(marca.getListaModelo(), ModeloDto::new);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    public List<ModeloDto> getListaModelo() {
//        return listaModelo;
//    }
//
//    public void setListaModelo(List<ModeloDto> listaModelo) {
//        this.listaModelo = listaModelo;
//    }
}
