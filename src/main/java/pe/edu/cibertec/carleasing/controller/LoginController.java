package pe.edu.cibertec.carleasing.controller;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.cibertec.carleasing.domain.Usuario;
import pe.edu.cibertec.carleasing.dto.LoginDto;
import pe.edu.cibertec.carleasing.mapper.UsuarioMapper;
import pe.edu.cibertec.carleasing.service.LoginService;

@RestController
@RequestMapping(path = "/login")
public class LoginController {

    @Autowired
    private UsuarioMapper usuarioMapper;
    @Autowired
    private LoginService loginService;

    @PostMapping
    public ResponseEntity<?> autorizar(
            @RequestBody LoginDto loginDto) {
        Usuario usuario = loginService.autorizar(loginDto.getEmail(), loginDto.getContrasena());
        if (usuario != null) {
            return ResponseEntity.ok(usuarioMapper.mapToUsuarioDto(usuario));
        }
        return ResponseEntity.status(UNAUTHORIZED).build();
    }
}
