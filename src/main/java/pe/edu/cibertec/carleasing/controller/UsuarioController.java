package pe.edu.cibertec.carleasing.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.cibertec.carleasing.domain.Usuario;
import pe.edu.cibertec.carleasing.dto.UsuarioDto;
import pe.edu.cibertec.carleasing.exception.InvalidResourceException;
import pe.edu.cibertec.carleasing.mapper.UsuarioMapper;
import pe.edu.cibertec.carleasing.service.UsuarioService;
import pe.edu.cibertec.carleasing.util.MapperUtils;

@RestController
@RequestMapping(path = "/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioMapper usuarioMapper;
    @Autowired
    private UsuarioService usuarioService;

    @GetMapping(path={"", "/"})
    public List<UsuarioDto> find(
        @RequestParam(name="dni", required=false) String dni) {
        return MapperUtils.mapList(
                usuarioService.findByDni(dni),
                usuarioMapper::mapToUsuarioDto
                );
    }

    @PostMapping
    public UsuarioDto save(
        @RequestBody UsuarioDto usuarioDto) {
        Usuario usuario = usuarioMapper.mapToUsuario(usuarioDto);
        usuarioService.save(usuario);
        UsuarioDto respuesta = usuarioMapper.mapToUsuarioDto(usuario);
        return respuesta;
    }

    @GetMapping(path={"/{id}", "/{id}/"})
    public UsuarioDto findById(
        @PathVariable(name="id") Long id) {
        Usuario usuario = usuarioService.findById(id);
        if (usuario == null)
            throw new InvalidResourceException(id, "usuario");
        return usuarioMapper.mapToUsuarioDto(usuario);
    }
}
