package pe.edu.cibertec.carleasing.controller.exceptionhandler;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import pe.edu.cibertec.carleasing.exception.BusinessRuleException;
import pe.edu.cibertec.carleasing.exception.DuplicatedElementException;
import pe.edu.cibertec.carleasing.exception.InvalidResourceException;

@ControllerAdvice
public class GlobalExceptionHandlerController {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandlerController.class);

    @ExceptionHandler(value = { Exception.class, RuntimeException.class })
    public ResponseEntity<?> defaultErrorHandler(HttpServletRequest request, Exception e) {
        Map<String, String> resultado = new LinkedHashMap<>();
        resultado.put("message", "Error al ejecutar operación. Revisar logs.");
        resultado.put("detailedMessage", e.getMessage());
        logger.error("Error al ejecutar operación. Revisar logs.", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resultado);
    }

    @ExceptionHandler(value=InvalidResourceException.class)
    public ResponseEntity<?> invalidResource(HttpServletRequest request, InvalidResourceException e) {
        logger.error("Recurso inválido. Lanzando 404.", e);
        Map<String, String> resultado = new LinkedHashMap<>();
        resultado.put("message", "El recurso no existe o no pudo ser identificado.");
        resultado.put("detailedMessage", e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resultado);
    }

    @ExceptionHandler(value={IllegalArgumentException.class, DuplicatedElementException.class, BusinessRuleException.class})
    public ResponseEntity<?> illegalArgument(HttpServletRequest request, RuntimeException e) {
        logger.error("Petición inválida. Lanzando 400.", e);
        Map<String, String> resultado = new LinkedHashMap<>();
        resultado.put("message", "Revisar los datos ingresados.");
        resultado.put("detailedMessage", e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
    }
}
