package pe.edu.cibertec.carleasing.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.cibertec.carleasing.dto.MarcaDto;
import pe.edu.cibertec.carleasing.dto.ModeloDto;
import pe.edu.cibertec.carleasing.service.MarcaService;
import pe.edu.cibertec.carleasing.service.ModeloService;
import pe.edu.cibertec.carleasing.util.MapperUtils;

@RestController
@RequestMapping(path = "/marcas",
    produces = APPLICATION_JSON_UTF8_VALUE,
    consumes = APPLICATION_JSON_UTF8_VALUE
)
public class MarcaController {

    @Autowired
    private MarcaService marcaService;
    @Autowired
    private ModeloService modeloService;

    @GetMapping(path={"", "/"})
    public List<MarcaDto> getAllMarcas() {
        return MapperUtils.mapList(marcaService.findAllMarcas(), MarcaDto::new);
    }

    @GetMapping(path={"{id}", "{id}/"})
    public ResponseEntity<?> getMarcaById(
            @PathVariable("id") Long id) {
        return MapperUtils.mapResponse(marcaService.findById(id), MarcaDto::new);
    }

    @GetMapping(path={"{id}/modelos", "{id}/modelos/"})
    public List<ModeloDto> getAllModelosOfMarca(
            @PathVariable("id") Long idMarca) {
        return MapperUtils.mapList(modeloService.findByMarca(idMarca), ModeloDto::new);
    }
}
