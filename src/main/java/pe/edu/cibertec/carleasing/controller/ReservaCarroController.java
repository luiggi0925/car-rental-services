package pe.edu.cibertec.carleasing.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.cibertec.carleasing.domain.ReservaCarro;
import pe.edu.cibertec.carleasing.service.ReservaCarroService;

@RestController
@RequestMapping("/reservas")
public class ReservaCarroController {

    @Autowired
    private ReservaCarroService reservaCarroService;

    @GetMapping(value={"/{id}", "/{id}/"})
    public ReservaCarro findById(@PathVariable("id") Long id) {
        return reservaCarroService.findById(id);
    }

    @PostMapping
    public ResponseEntity<ReservaCarro> save(
            @RequestBody ReservaCarro reservaCarro) {
        reservaCarroService.save(reservaCarro);
        return ResponseEntity.ok(reservaCarro);
    }

    @DeleteMapping(value={"/{id}", "/{id}/"})
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        reservaCarroService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value={"", "/"})
    public List<ReservaCarro> findByParameters(
        @RequestParam(name="idCarro", required=false) Long idCarro,
        @RequestParam(name="idCliente", required=false) Long idCliente) {
        if (idCarro != null) {
            return reservaCarroService.findByCarro(idCarro);
        } else if (idCliente != null) {
            return reservaCarroService.findByCliente(idCliente);
        }
        return Collections.emptyList();
    }

    @PutMapping(value={"/{id}/calificacion", "/{id}/calificacion/"})
    public ResponseEntity<ReservaCarro> setRate(
            @PathVariable("id") Long id,
            @RequestBody ReservaCarro reservaCarro) {
        reservaCarroService.setRate(reservaCarro);
        return ResponseEntity.ok(reservaCarro);
    }
}
