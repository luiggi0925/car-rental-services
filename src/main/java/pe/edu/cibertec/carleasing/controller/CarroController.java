package pe.edu.cibertec.carleasing.controller;

import static java.util.stream.Collectors.toList;
import static pe.edu.cibertec.carleasing.util.RequestResponseUtils.fileDownload;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.cibertec.carleasing.domain.Carro;
import pe.edu.cibertec.carleasing.domain.ImagenCarro;
import pe.edu.cibertec.carleasing.domain.base.Pagina;
import pe.edu.cibertec.carleasing.domain.search.BusquedaCarro;
import pe.edu.cibertec.carleasing.domain.search.OrdenCarro;
import pe.edu.cibertec.carleasing.dto.CarroDto;
import pe.edu.cibertec.carleasing.exception.InvalidResourceException;
import pe.edu.cibertec.carleasing.mapper.CarroMapper;
import pe.edu.cibertec.carleasing.service.CarroService;
import pe.edu.cibertec.carleasing.util.Constantes;
import pe.edu.cibertec.carleasing.util.MapperUtils;

@RestController
@RequestMapping("/carros")
public class CarroController {

    @Autowired
    private CarroService carroService;

    @Autowired
    private CarroMapper carroMapper;

    @GetMapping(path={"", "/"})
    public Pagina<CarroDto> busquedaCarros(
            @RequestParam(name="idUsuario", required=false, defaultValue="0") Long idUsuario,
            @RequestParam(name="idMarca", required=false, defaultValue="0") Long idMarca,
            @RequestParam(name="idModelo", required=false, defaultValue="0") Long idModelo,
            @RequestParam(name="anioDesde", required=false, defaultValue="1990") int anioDesde,
            @RequestParam(name="detalle", required=false) String detalle,
            @RequestParam(name="pasajeros", required=false, defaultValue="0") int pasajeros,
            @RequestParam(name="precioMinimo", required=false, defaultValue="0") BigDecimal precioMinimo,
            @RequestParam(name="precioMaximo", required=false, defaultValue="1000000") BigDecimal precioMaximo,
            @RequestParam(name="orden", required=false, defaultValue="1") int orden,
            @RequestParam(name="desde", required=false, defaultValue="0") int desde,
            @RequestParam(name="hasta", required=false, defaultValue="0") int hasta) {
        hasta = hasta == 0 ? Constantes.TAMANIO_BASE_PAGINA - 1 : hasta;
        return MapperUtils.mapPagina(carroService.findByParameters(
                new BusquedaCarro(idUsuario, idMarca, idModelo, anioDesde, detalle, pasajeros, precioMinimo, precioMaximo, OrdenCarro.getByOrdenValue(orden), desde, hasta)
            ), carroMapper::mapToCarroDto);
    }

    @GetMapping(path={"/{id}", "/{id}/"})
    public CarroDto findById(
            @PathVariable(name="id", required=true) Long id) {
        Carro carro = carroService.findById(id);
        if (carro == null)
            throw new InvalidResourceException(id, "carro");
        return carroMapper.mapToCarroDto(carro);
    }

    @PostMapping
    public ResponseEntity<?> save(
            @ModelAttribute CarroDto carroDto) {
        carroDto.setId(null);
        Carro carro = carroMapper.mapToCarro(carroDto);
        carroService.save(carro);
        CarroDto carroDtoRespose = carroMapper.mapToCarroDto(carro);
        return ResponseEntity.ok(carroDtoRespose);
    }

    @PutMapping
    public ResponseEntity<?> update(
            @ModelAttribute CarroDto carroDto) {
        Carro carro = carroMapper.mapToCarro(carroDto);
        carroService.save(carro);
        CarroDto carroDtoRespose = carroMapper.mapToCarroDto(carro);
        return ResponseEntity.ok(carroDtoRespose);
    }

    @GetMapping(path={"/{id}/imagenes", "/{id}/imagenes/"})
    public List<String> obtenerRutasImagenes(@PathVariable("id") Long idCarro) {
        List<ImagenCarro> listaImagenCarro = carroService.findImagenCarroByIdCarro(idCarro);
        return listaImagenCarro.stream()
                .map(carroMapper::mapToRutaImagen)
                .collect(toList());
    }

    @GetMapping(path={"/{id}/imagenes/{idImagenCarro}", "/{id}/imagenes/{idImagenCarro}/"})
    public ResponseEntity<Void> descargarImagenCarro(
            @PathVariable("id") Long idCarro,
            @PathVariable("idImagenCarro") Long idImagenCarro,
            HttpServletResponse response) {
        ImagenCarro imagenCarro = carroService.findImagenCarroById(idCarro, idImagenCarro);
        if (imagenCarro == null)
            throw new InvalidResourceException(idImagenCarro, "imagen carro");
        try {
            fileDownload(imagenCarro.getImagen(), imagenCarro.getNombre(), response);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Error al tratar de descargar imagen de carro con id %d", idImagenCarro), e);
        }
        return ResponseEntity.ok().build();
    }
}
