package pe.edu.cibertec.carleasing.service;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.Modelo;

public interface ModeloService {

    List<Modelo> findByMarca(Long idMarca);
}
