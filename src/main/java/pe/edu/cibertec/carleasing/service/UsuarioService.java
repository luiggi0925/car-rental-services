package pe.edu.cibertec.carleasing.service;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.Usuario;

public interface UsuarioService {

    List<Usuario> findByDni(String dni);
    List<Usuario> findAll();
    void save(Usuario usuario);
    Usuario findById(Long id);
    Usuario findByEmail(String email);
}
