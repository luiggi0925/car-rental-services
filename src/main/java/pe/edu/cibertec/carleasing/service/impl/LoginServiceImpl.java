package pe.edu.cibertec.carleasing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.cibertec.carleasing.domain.Usuario;
import pe.edu.cibertec.carleasing.service.LoginService;
import pe.edu.cibertec.carleasing.service.UsuarioService;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UsuarioService usuarioService;

    @Override
    public Usuario autorizar(String email, String contrasena) {
        Usuario usuario = usuarioService.findByEmail(email);
        if (usuario.getContrasena().equals(contrasena)) {
            return usuario;
        }
        return null;
    }
}
