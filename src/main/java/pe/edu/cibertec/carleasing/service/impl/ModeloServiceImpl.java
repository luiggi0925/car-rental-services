package pe.edu.cibertec.carleasing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.cibertec.carleasing.domain.Modelo;
import pe.edu.cibertec.carleasing.repository.ModeloRepository;
import pe.edu.cibertec.carleasing.service.ModeloService;

@Service
public class ModeloServiceImpl implements ModeloService {

    @Autowired
    private ModeloRepository modeloRepository;

    @Override
    public List<Modelo> findByMarca(Long idMarca) {
        return modeloRepository.findByMarca(idMarca);
    }
}
