package pe.edu.cibertec.carleasing.service.impl;

import static java.util.Collections.emptyList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.edu.cibertec.carleasing.domain.Carro;
import pe.edu.cibertec.carleasing.domain.ImagenCarro;
import pe.edu.cibertec.carleasing.domain.base.Pagina;
import pe.edu.cibertec.carleasing.domain.search.BusquedaCarro;
import pe.edu.cibertec.carleasing.repository.CarroRepository;
import pe.edu.cibertec.carleasing.repository.ImagenCarroRepository;
import pe.edu.cibertec.carleasing.service.CarroService;

@Service
public class CarroServiceImpl implements CarroService {

    @Autowired
    @Qualifier("carroRepositoryJpa")
    private CarroRepository carroRepository;
    @Autowired
    @Qualifier("imagenCarroRepositoryJpa")
    private ImagenCarroRepository imagenCarroRepository;

    @Override
    public Carro findById(Long id) {
        return carroRepository.getPorId(id);
    }

    private Pagina<Carro> findByUserId(Long idUsuario, int desde, int hasta) {
        return carroRepository.findByUserId(idUsuario, desde, hasta);
    }

    @Transactional
    @Override
    public void save(Carro carro) {
        carro.getListaImagenCarro().forEach(imagenCarro -> imagenCarro.setCarro(carro));
        carro.setDisponible(true);
        carroRepository.guardar(carro);
    }

    @Transactional
    @Override
    public void update(Carro carro) {
        if (carro.getId() == null) {
            throw new IllegalArgumentException("Id no puede ser nulo");
        }
        carroRepository.actualizar(carro);
    }

    @Override
    public List<ImagenCarro> findImagenCarroByIdCarro(Long idCarro) {
        return Optional.ofNullable(imagenCarroRepository.findImagenCarroByIdCarro(idCarro))
            .orElse(emptyList());
    }

    @Override
    public ImagenCarro findImagenCarroById(Long idCarro, Long idImagenCarro) {
        return imagenCarroRepository.findByIdAndIdCarro(idCarro, idImagenCarro);
    }

    @Override
    public Pagina<Carro> findByParameters(BusquedaCarro busquedaCarro) {
        return carroRepository.findByParameters(busquedaCarro);
    }

    @Override
    public Pagina<Carro> getPaginaTodos(int desde, int hasta) {
        return carroRepository.getPagina(desde, hasta);
    }
}
