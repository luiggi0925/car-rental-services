package pe.edu.cibertec.carleasing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import pe.edu.cibertec.carleasing.domain.Marca;
import pe.edu.cibertec.carleasing.repository.MarcaRepository;
import pe.edu.cibertec.carleasing.service.MarcaService;
import pe.edu.cibertec.carleasing.util.MapperUtils;

@Service
public class MarcaServiceImpl implements MarcaService {

    @Autowired
    @Qualifier("marcaRepositoryJpa")
    private MarcaRepository marcaRepository;

    @Override
    public List<Marca> findAllMarcas() {
        return MapperUtils.asList(marcaRepository.getTodos());
    }

    @Override
    public Marca findById(Long id) {
        return marcaRepository.getPorId(id);
    }
}
