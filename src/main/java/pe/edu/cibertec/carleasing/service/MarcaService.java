package pe.edu.cibertec.carleasing.service;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.Marca;

public interface MarcaService {

    List<Marca> findAllMarcas();

    Marca findById(Long id);
}
