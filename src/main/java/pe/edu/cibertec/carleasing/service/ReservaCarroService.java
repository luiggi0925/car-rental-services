package pe.edu.cibertec.carleasing.service;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.ReservaCarro;

public interface ReservaCarroService {

    void save(ReservaCarro reservaCarro);
    void delete(Long idReservaCarro);
    List<ReservaCarro> findByCarro (Long idCarro);
    List<ReservaCarro> findByCliente (Long idCliente);
    ReservaCarro findById(Long id);
    void setRate(ReservaCarro reservaCarro);
    void update(ReservaCarro reservaCarro);
}
