package pe.edu.cibertec.carleasing.service;

import java.util.List;

import pe.edu.cibertec.carleasing.domain.Carro;
import pe.edu.cibertec.carleasing.domain.ImagenCarro;
import pe.edu.cibertec.carleasing.domain.base.Pagina;
import pe.edu.cibertec.carleasing.domain.search.BusquedaCarro;

public interface CarroService {

    Pagina<Carro> getPaginaTodos(int desde, int hasta);
    default Pagina<Carro> getPaginaTodos() {
        return getPaginaTodos(0, 10);
    }

    Carro findById(Long id);

    void save(Carro carro);

    void update(Carro carro);

    List<ImagenCarro> findImagenCarroByIdCarro(Long idCarro);

    ImagenCarro findImagenCarroById(Long idCarro, Long idImagenCarro);

    Pagina<Carro> findByParameters(BusquedaCarro parametrosBusqueda);
}
