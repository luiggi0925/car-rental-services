package pe.edu.cibertec.carleasing.service.impl;

import static pe.edu.cibertec.carleasing.domain.status.EstadoReservaCarro.CANCELADO;
import static pe.edu.cibertec.carleasing.domain.status.EstadoReservaCarro.EN_CURSO;
import static pe.edu.cibertec.carleasing.domain.status.EstadoReservaCarro.PENDIENTE;
import static pe.edu.cibertec.carleasing.domain.status.EstadoReservaCarro.TERMINADA;
import static pe.edu.cibertec.carleasing.util.ObjetoUtils.antesDe;
import static pe.edu.cibertec.carleasing.util.ObjetoUtils.antesDeHoy;
import static pe.edu.cibertec.carleasing.util.ObjetoUtils.getDiasEntre;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.edu.cibertec.carleasing.domain.Carro;
import pe.edu.cibertec.carleasing.domain.ReservaCarro;
import pe.edu.cibertec.carleasing.exception.BusinessRuleException;
import pe.edu.cibertec.carleasing.repository.ReservaCarroRepository;
import pe.edu.cibertec.carleasing.service.CarroService;
import pe.edu.cibertec.carleasing.service.ReservaCarroService;

@Service
public class ReservaCarroServiceImpl implements ReservaCarroService {

    @Autowired
    private ReservaCarroRepository reservaCarroRepository;
    @Autowired
    private CarroService carroService;

    private void validarFechas(ReservaCarro reservaCarro) {
        if (antesDeHoy(reservaCarro.getFechaInicio()) )
            throw new BusinessRuleException("La reserva no puede iniciar antes del día actual.");
        if (antesDeHoy(reservaCarro.getFechaFin()) )
            throw new BusinessRuleException("La reserva no puede terminar antes del día actual.");
        if (antesDe(reservaCarro.getFechaFin(), reservaCarro.getFechaInicio()) )
            throw new BusinessRuleException("La fecha de fin de la reserva no puede ser antes de la fecha de inicio.");
        long cantidadFechas = reservaCarroRepository.obtenerCantidadReservasParaFechas(reservaCarro);
        if (cantidadFechas > 0)
            throw new BusinessRuleException("Existen reservas para el carro en el rango de fechas indicado.");
    }

    private BigDecimal calculaPrecioTotal(ReservaCarro reservaCarro) {
        BigDecimal diasReserva = new BigDecimal(getDiasEntre(reservaCarro.getFechaInicio(), reservaCarro.getFechaFin()));
        return reservaCarro.getPrecioDiario().multiply(diasReserva);
    }

    @Transactional
    @Override
    public void save(ReservaCarro reservaCarro) {
        if (reservaCarro.getIdCarro() == null)
            throw new IllegalArgumentException("Debe indicarse el carro a reservar.");
        if (reservaCarro.getCliente() == null || reservaCarro.getCliente().getId() == null)
            throw new IllegalArgumentException("La reserva no posee un cliente.");
        if (reservaCarro.getFechaInicio() == null)
            throw new IllegalArgumentException("La reserva no tiene fecha de inicio.");
        if (reservaCarro.getFechaFin() == null)
            throw new IllegalArgumentException("La reserva no tiene fecha de fin");
        
        validarFechas(reservaCarro);

        Carro carro = carroService.findById(reservaCarro.getIdCarro());
        if (carro == null)
            throw new BusinessRuleException("El carro no existe.");
        if (!carro.isDisponible())
            throw new BusinessRuleException("El carro no se encuentra disponible.");
        if (carro.getDuenio().getId().equals(reservaCarro.getCliente().getId()))
            throw new BusinessRuleException("El carro no puede ser reservado por su duenio");

        reservaCarro.setPrecioDiario(carro.getPrecioDiario());
        reservaCarro.setPrecioTotal(calculaPrecioTotal(reservaCarro));
        reservaCarroRepository.guardar(reservaCarro);
    }

    @Transactional
    @Override
    public void delete(Long idReservaCarro) {
        ReservaCarro reservaCarro = reservaCarroRepository.getPorId(idReservaCarro);
        if (reservaCarro.getEstado() == TERMINADA)
            throw new BusinessRuleException("La reserva ya terminó. No se puede cancelar.");
        if (reservaCarro.getEstado() == CANCELADO)
            throw new BusinessRuleException("La reserva ya está cancelada. No se puede cancelar.");
        if (reservaCarro.getEstado() == EN_CURSO) {
            Carro carro = carroService.findById(reservaCarro.getIdCarro());
            carro.setDisponible(true);
            carroService.update(carro);
        }
        reservaCarroRepository.eliminar(idReservaCarro);
    }

    @Override
    public ReservaCarro findById(Long id) {
        return reservaCarroRepository.getPorId(id);
    }

    @Override
    public List<ReservaCarro> findByCarro(Long idCarro) {
        return reservaCarroRepository.obtenerPorCarro(idCarro);
    }

    @Override
    public List<ReservaCarro> findByCliente(Long idCliente) {
        return reservaCarroRepository.obtenerPorCliente(idCliente);
    }

    private boolean esEstadoFinal(ReservaCarro reservaCarro) {
        return reservaCarro.getEstado() == CANCELADO ||
                reservaCarro.getEstado() == TERMINADA;
    }

    static final int PUNTAJE_MINIMO = 1;
    static final int PUNTAJE_MAXIMO = 5;

    @Transactional
    @Override
    public void update(ReservaCarro reservaCarro) {
//        validarFechas(reservaCarro);
        

        ReservaCarro actual = reservaCarroRepository.getPorId(reservaCarro.getId());
        if (esEstadoFinal(actual)) {
            if (reservaCarro.getEstado() != null
                    && reservaCarro.getEstado() != actual.getEstado())
                throw new BusinessRuleException("La reserva ha finalizado. Está terminada o cancelada.");
            reservaCarro.setEstado(null);
        } else {
            if (actual.getEstado() == EN_CURSO && reservaCarro.getEstado() == PENDIENTE)
                throw new BusinessRuleException("La reserva está en curso. No puede cambiarse a pendiente.");
        }
        if (reservaCarro.getPuntaje() < PUNTAJE_MINIMO || reservaCarro.getPuntaje() > PUNTAJE_MAXIMO)
            throw new BusinessRuleException("El puntaje de la reserva está fuera de rango. El valor debe ser entre 1 y 5.");
        if (reservaCarro.getCliente() != null &&
                !actual.getCliente().getId().equals(reservaCarro.getCliente().getId()))
            throw new BusinessRuleException("No se puede cambiar de cliente de la reserva. Elimine la reserva y cree una nueva.");

        reservaCarro.setPrecioDiario(actual.getPrecioDiario());
        reservaCarro.setPrecioTotal(calculaPrecioTotal(reservaCarro));
        reservaCarroRepository.actualizar(reservaCarro);
    }

    @Transactional
    @Override
    public void setRate(ReservaCarro reservaCarro) {
        reservaCarroRepository.actualizarCalificacion(reservaCarro);
    }
}
