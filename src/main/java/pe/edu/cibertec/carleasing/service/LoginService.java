package pe.edu.cibertec.carleasing.service;

import pe.edu.cibertec.carleasing.domain.Usuario;

public interface LoginService {

    Usuario autorizar(String email, String contrasena);

}