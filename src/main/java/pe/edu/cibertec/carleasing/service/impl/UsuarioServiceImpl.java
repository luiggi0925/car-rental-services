package pe.edu.cibertec.carleasing.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.edu.cibertec.carleasing.domain.Usuario;
import pe.edu.cibertec.carleasing.exception.DuplicatedElementException;
import pe.edu.cibertec.carleasing.repository.UsuarioRepository;
import pe.edu.cibertec.carleasing.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    @Qualifier("usuarioRepositoryJpa")
    private UsuarioRepository usuarioRepository;

    @Override
    public List<Usuario> findByDni(String dni) {
        if (StringUtils.isNotEmpty(dni)) {
            return usuarioRepository.findByDni(dni);
        }
        return usuarioRepository.getTodos();
    }

    @Override
    public List<Usuario> findAll() {
        return usuarioRepository.getTodos();
    }

    @Transactional
    @Override
    public void save(Usuario usuario) {
        try {
            usuarioRepository.guardar(usuario);
        } catch (DataIntegrityViolationException dive) {
            Throwable realCause = dive.getCause();
            while (realCause.getCause() != null) {
                realCause = realCause.getCause();
            }
//            if (realCause instanceof PSQLException) {
//                PSQLException postgreException = (PSQLException) realCause;
//                if ("23505".equals(postgreException.getSQLState())) {
//                    String message = postgreException.getMessage().contains("usuario_dni_key") ?
//                            String.format("Ya existe un usuario con dni %s.", usuario.getDni()) :
//                            String.format("Ya existe un usuario con correo %s.", usuario.getEmail());
//                    throw new DuplicatedElementException(message);
//                }
//            }
            if (realCause instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException) {
                throw new DuplicatedElementException(realCause.getMessage(), realCause);
            }
            throw new RuntimeException(dive);
        }
    }

    @Override
    public Usuario findById(Long id) {
        return usuarioRepository.getPorId(id);
    }

    @Override
    public Usuario findByEmail(String email) {
        return usuarioRepository.findByEmail(email);
    }
}
