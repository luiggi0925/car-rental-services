-- Usuarios
insert into usuario (nombre, apellido, email, fechanacimiento, dni, contrasena)
values ('Luiggi', 'Mendoza', 'luiggi0925@gmail.com', '1987-12-01', '12345678', 'luiggi');
insert into usuario (nombre, apellido, email, fechanacimiento, dni, contrasena)
values ('Mary', 'Garcia', 'mary2707@gmail.com', '1985-11-29', '11223344', 'mary');

-- Marcas
insert into marca (nombre) values ('Audi');
insert into marca (nombre) values ('BMW');
insert into marca (nombre) values ('Cadillac');
insert into marca (nombre) values ('Chevrolet');
insert into marca (nombre) values ('FIAT');
insert into marca (nombre) values ('Ford');
insert into marca (nombre) values ('Honda');
insert into marca (nombre) values ('Hyundai');
insert into marca (nombre) values ('Jaguar');
insert into marca (nombre) values ('Kia');
insert into marca (nombre) values ('Land Rover');
insert into marca (nombre) values ('Mazda');
insert into marca (nombre) values ('Mercedes-Benz');
insert into marca (nombre) values ('Mercury');
insert into marca (nombre) values ('Mitsubishi');
insert into marca (nombre) values ('Nissan');
insert into marca (nombre) values ('Porsche');
insert into marca (nombre) values ('Subaru');
insert into marca (nombre) values ('Suzuki');
insert into marca (nombre) values ('Toyota');
insert into marca (nombre) values ('Volkswagen');
insert into marca (nombre) values ('Volvo');


-- Audi
insert into modelo (nombre, idmarca) values ('A3 Convertible', 1);
insert into modelo (nombre, idmarca) values ('A3 Diesel', 1);
insert into modelo (nombre, idmarca) values ('A3 Sedan', 1);
insert into modelo (nombre, idmarca) values ('A3 Wagon', 1);
insert into modelo (nombre, idmarca) values ('A4 Sedan', 1);
insert into modelo (nombre, idmarca) values ('A4 Wagon', 1);
insert into modelo (nombre, idmarca) values ('A5 Convertible', 1);
insert into modelo (nombre, idmarca) values ('A5 Coupe', 1);
insert into modelo (nombre, idmarca) values ('A6 Diesel', 1);
insert into modelo (nombre, idmarca) values ('A6 Sedan', 1);
insert into modelo (nombre, idmarca) values ('A6 Wagon', 1);
insert into modelo (nombre, idmarca) values ('A7 Diesel', 1);
insert into modelo (nombre, idmarca) values ('A7 Sedan', 1);
insert into modelo (nombre, idmarca) values ('A8 Diesel', 1);
insert into modelo (nombre, idmarca) values ('A8 Sedan', 1);
insert into modelo (nombre, idmarca) values ('allroallroad Wagon', 1);
insert into modelo (nombre, idmarca) values ('Q3 SUV', 1);
insert into modelo (nombre, idmarca) values ('Q5 Diesel', 1);
insert into modelo (nombre, idmarca) values ('Q5 Hybrid', 1);
insert into modelo (nombre, idmarca) values ('Q5 SUV', 1);
insert into modelo (nombre, idmarca) values ('Q7 Diesel', 1);
insert into modelo (nombre, idmarca) values ('Q7 SUV', 1);
insert into modelo (nombre, idmarca) values ('R8 Convertible', 1);
insert into modelo (nombre, idmarca) values ('R8 Coupe', 1);
insert into modelo (nombre, idmarca) values ('R8 GT 5.2 FSI quattro', 1);
insert into modelo (nombre, idmarca) values ('R8 GT 5.2 FSI quattro Spyder', 1);
insert into modelo (nombre, idmarca) values ('RS 5 Convertible', 1);
insert into modelo (nombre, idmarca) values ('RS 5 Coupe', 1);
insert into modelo (nombre, idmarca) values ('RS 7 Sedan', 1);
insert into modelo (nombre, idmarca) values ('S3 Sedan', 1);
insert into modelo (nombre, idmarca) values ('S4 Sedan', 1);
insert into modelo (nombre, idmarca) values ('S5 Convertible', 1);
insert into modelo (nombre, idmarca) values ('S5 Coupe', 1);
insert into modelo (nombre, idmarca) values ('S6 Sedan', 1);
insert into modelo (nombre, idmarca) values ('S7 Sedan', 1);
insert into modelo (nombre, idmarca) values ('S8 Sedan', 1);
insert into modelo (nombre, idmarca) values ('SQ5 SUV', 1);
insert into modelo (nombre, idmarca) values ('TT Convertible', 1);
insert into modelo (nombre, idmarca) values ('TT Coupe', 1);
insert into modelo (nombre, idmarca) values ('TT Hatchback', 1);
insert into modelo (nombre, idmarca) values ('TT RS Coupe', 1);
insert into modelo (nombre, idmarca) values ('TTS Convertible', 1);
insert into modelo (nombre, idmarca) values ('TTS Coupe', 1);
insert into modelo (nombre, idmarca) values ('TTS Hatchback', 1);

-- bmw
insert into modelo (idmarca, nombre) values (2, '1 Series Convertible');
insert into modelo (idmarca, nombre) values (2, '1 Series Coupe');
insert into modelo (idmarca, nombre) values (2, '2 Series Convertible');
insert into modelo (idmarca, nombre) values (2, '2 Series Coupe');
insert into modelo (idmarca, nombre) values (2, '2 Series M235i');
insert into modelo (idmarca, nombre) values (2, '2 Series M235i xDrive');
insert into modelo (idmarca, nombre) values (2, '2 Series M240i');
insert into modelo (idmarca, nombre) values (2, '2 Series M240i xDrive');
insert into modelo (idmarca, nombre) values (2, '3 Series Convertible');
insert into modelo (idmarca, nombre) values (2, '3 Series Coupe');
insert into modelo (idmarca, nombre) values (2, '3 Series Diesel');
insert into modelo (idmarca, nombre) values (2, '3 Series Hybrid');
insert into modelo (idmarca, nombre) values (2, '3 Series Sedan');
insert into modelo (idmarca, nombre) values (2, '3 Series Wagon');
insert into modelo (idmarca, nombre) values (2, '3 Series Gran Turismo Hatchback');
insert into modelo (idmarca, nombre) values (2, '4 Series Convertible');
insert into modelo (idmarca, nombre) values (2, '4 Series Coupe');
insert into modelo (idmarca, nombre) values (2, '4 Series Gran Coupe Sedan');
insert into modelo (idmarca, nombre) values (2, '5 Series Diesel');
insert into modelo (idmarca, nombre) values (2, '5 Series Hybrid');
insert into modelo (idmarca, nombre) values (2, '5 Series Sedan');
insert into modelo (idmarca, nombre) values (2, '5 Series Gran Turismo Hatchback');
insert into modelo (idmarca, nombre) values (2, '6 Series Convertible');
insert into modelo (idmarca, nombre) values (2, '6 Series Coupe');
insert into modelo (idmarca, nombre) values (2, '6 Series Gran Coupe Sedan');
insert into modelo (idmarca, nombre) values (2, '7 Series Diesel');
insert into modelo (idmarca, nombre) values (2, '7 Series Sedan');
insert into modelo (idmarca, nombre) values (2, 'ActiveHybrid 5 Sedan');
insert into modelo (idmarca, nombre) values (2, 'ActiveHybrid 7 Sedan');
insert into modelo (idmarca, nombre) values (2, 'ALPINA B6 Gran Coupe Sedan');
insert into modelo (idmarca, nombre) values (2, 'ALPINA B7 Sedan');
insert into modelo (idmarca, nombre) values (2, 'i3 Hatchback');
insert into modelo (idmarca, nombre) values (2, 'M3 Convertible');
insert into modelo (idmarca, nombre) values (2, 'M3 Coupe');
insert into modelo (idmarca, nombre) values (2, 'M3 Sedan');
insert into modelo (idmarca, nombre) values (2, 'M4 Convertible');
insert into modelo (idmarca, nombre) values (2, 'M4 Coupe');
insert into modelo (idmarca, nombre) values (2, 'M5 Sedan');
insert into modelo (idmarca, nombre) values (2, 'M6 Convertible');
insert into modelo (idmarca, nombre) values (2, 'M6 Coupe');
insert into modelo (idmarca, nombre) values (2, 'M6 Gran Coupe Sedan');
insert into modelo (idmarca, nombre) values (2, 'X1 SUV');
insert into modelo (idmarca, nombre) values (2, 'X3 Diesel');
insert into modelo (idmarca, nombre) values (2, 'X3 SUV');
insert into modelo (idmarca, nombre) values (2, 'X4 SUV');
insert into modelo (idmarca, nombre) values (2, 'X5 Diesel');
insert into modelo (idmarca, nombre) values (2, 'X5 SUV');
insert into modelo (idmarca, nombre) values (2, 'X5 M SUV');
insert into modelo (idmarca, nombre) values (2, 'X6 SUV');
insert into modelo (idmarca, nombre) values (2, 'X6 M SUV');
insert into modelo (idmarca, nombre) values (2, 'Z4 Convertible');


-- cadillac
insert into modelo (idmarca, nombre) values (3, 'ATS Sedan');
insert into modelo (idmarca, nombre) values (3, 'ATS Coupe');
insert into modelo (idmarca, nombre) values (3, 'ATS-V Coupe');
insert into modelo (idmarca, nombre) values (3, 'ATS-V Sedan');
insert into modelo (idmarca, nombre) values (3, 'CT6 Sedan');
insert into modelo (idmarca, nombre) values (3, 'CTS Sedan');
insert into modelo (idmarca, nombre) values (3, 'CTS V-Sport Premium Luxury');
insert into modelo (idmarca, nombre) values (3, 'CTS Vsport');
insert into modelo (idmarca, nombre) values (3, 'CTS Vsport Premium');
insert into modelo (idmarca, nombre) values (3, 'CTS Coupe');
insert into modelo (idmarca, nombre) values (3, 'CTS-V Sedan');
insert into modelo (idmarca, nombre) values (3, 'CTS-V Coupe');
insert into modelo (idmarca, nombre) values (3, 'CTS-V Wagon');
insert into modelo (idmarca, nombre) values (3, 'CTS Wagon');
insert into modelo (idmarca, nombre) values (3, 'DTS Sedan');
insert into modelo (idmarca, nombre) values (3, 'Escalade SUV');
insert into modelo (idmarca, nombre) values (3, 'Escalade ESV SUV');
insert into modelo (idmarca, nombre) values (3, 'Escalade EXT Crew Cab');
insert into modelo (idmarca, nombre) values (3, 'Escalade Hybrid SUV');
insert into modelo (idmarca, nombre) values (3, 'SRX SUV');
insert into modelo (idmarca, nombre) values (3, 'STS Sedan');
insert into modelo (idmarca, nombre) values (3, 'XT5 SUV');
insert into modelo (idmarca, nombre) values (3, 'XTS Sedan');


-- Chevrolet
insert into modelo (idmarca, nombre) values (4, 'Avalanche Crew Cab');
insert into modelo (idmarca, nombre) values (4, 'Aveo Hatchback');
insert into modelo (idmarca, nombre) values (4, 'Aveo Sedan');
insert into modelo (idmarca, nombre) values (4, 'Black Diamond Avalanche Crew Cab');
insert into modelo (idmarca, nombre) values (4, 'Camaro Convertible');
insert into modelo (idmarca, nombre) values (4, 'Camaro Coupe');
insert into modelo (idmarca, nombre) values (4, 'Camaro Z/28');
insert into modelo (idmarca, nombre) values (4, 'Camaro ZL1');
insert into modelo (idmarca, nombre) values (4, 'Colorado Crew Cab');
insert into modelo (idmarca, nombre) values (4, 'Colorado Extended Cab');
insert into modelo (idmarca, nombre) values (4, 'Colorado Regular Cab');
insert into modelo (idmarca, nombre) values (4, 'Corvette 427');
insert into modelo (idmarca, nombre) values (4, 'Corvette Convertible');
insert into modelo (idmarca, nombre) values (4, 'Corvette Coupe');
insert into modelo (idmarca, nombre) values (4, 'Corvette Z06');
insert into modelo (idmarca, nombre) values (4, 'Corvette Z06 w/1LZ');
insert into modelo (idmarca, nombre) values (4, 'Corvette Z06 w/2LZ');
insert into modelo (idmarca, nombre) values (4, 'Corvette Z06 w/3LZ');
insert into modelo (idmarca, nombre) values (4, 'Corvette ZR1');
insert into modelo (idmarca, nombre) values (4, 'Corvette Stingray Convertible');
insert into modelo (idmarca, nombre) values (4, 'Corvette Stingray Coupe');
insert into modelo (idmarca, nombre) values (4, 'Cruze Diesel');
insert into modelo (idmarca, nombre) values (4, 'Cruze Sedan');
insert into modelo (idmarca, nombre) values (4, 'Cruze Limited Sedan');
insert into modelo (idmarca, nombre) values (4, 'Equinox SUV');
insert into modelo (idmarca, nombre) values (4, 'Express Diesel');
insert into modelo (idmarca, nombre) values (4, 'Express Van');
insert into modelo (idmarca, nombre) values (4, 'Express Cargo Diesel');
insert into modelo (idmarca, nombre) values (4, 'Express Cargo Van');
insert into modelo (idmarca, nombre) values (4, 'HHR Wagon');
insert into modelo (idmarca, nombre) values (4, 'Impala Hybrid');
insert into modelo (idmarca, nombre) values (4, 'Impala Sedan');
insert into modelo (idmarca, nombre) values (4, 'Malibu Hybrid');
insert into modelo (idmarca, nombre) values (4, 'Malibu Sedan');
insert into modelo (idmarca, nombre) values (4, 'Malibu Limited Sedan');
insert into modelo (idmarca, nombre) values (4, 'Silverado 1500 Crew Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 1500 Double Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 1500 Extended Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 1500 Regular Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 1500 Hybrid Crew Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 2500HD Crew Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 2500HD Double Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 2500HD Extended Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 2500HD Regular Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 3500HD Crew Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 3500HD Double Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 3500HD Extended Cab');
insert into modelo (idmarca, nombre) values (4, 'Silverado 3500HD Regular Cab');
insert into modelo (idmarca, nombre) values (4, 'Sonic Hatchback');
insert into modelo (idmarca, nombre) values (4, 'Sonic Sedan');
insert into modelo (idmarca, nombre) values (4, 'Spark Hatchback');
insert into modelo (idmarca, nombre) values (4, 'Spark EV Hatchback');
insert into modelo (idmarca, nombre) values (4, 'SS Sedan');
insert into modelo (idmarca, nombre) values (4, 'Suburban SUV');
insert into modelo (idmarca, nombre) values (4, 'Tahoe SUV');
insert into modelo (idmarca, nombre) values (4, 'Tahoe Hybrid SUV');
insert into modelo (idmarca, nombre) values (4, 'Traverse SUV');
insert into modelo (idmarca, nombre) values (4, 'Trax SUV');
insert into modelo (idmarca, nombre) values (4, 'Volt Hatchback');


-- fiat
insert into modelo (idmarca, nombre) values (5, '500 Abarth');
insert into modelo (idmarca, nombre) values (5, '500 C Abarth');
insert into modelo (idmarca, nombre) values (5, '500 Convertible');
insert into modelo (idmarca, nombre) values (5, '500 Hatchback');
insert into modelo (idmarca, nombre) values (5, '500e Hatchback');
insert into modelo (idmarca, nombre) values (5, '500L Wagon');


-- ford
insert into modelo (idmarca, nombre) values (6, 'C-Max Energi Wagon');
insert into modelo (idmarca, nombre) values (6, 'C-Max Hybrid Wagon');
insert into modelo (idmarca, nombre) values (6, 'Edge SUV');
insert into modelo (idmarca, nombre) values (6, 'Escape Hybrid');
insert into modelo (idmarca, nombre) values (6, 'Escape SUV');
insert into modelo (idmarca, nombre) values (6, 'Escape Hybrid SUV');
insert into modelo (idmarca, nombre) values (6, 'E-Series Van');
insert into modelo (idmarca, nombre) values (6, 'E-Series Wagon Van');
insert into modelo (idmarca, nombre) values (6, 'Expedition SUV');
insert into modelo (idmarca, nombre) values (6, 'Explorer SUV');
insert into modelo (idmarca, nombre) values (6, 'F-150 Regular Cab');
insert into modelo (idmarca, nombre) values (6, 'F-150 SVT Raptor');
insert into modelo (idmarca, nombre) values (6, 'F-150 SuperCab');
insert into modelo (idmarca, nombre) values (6, 'F-150 SuperCrew');
insert into modelo (idmarca, nombre) values (6, 'F-250 Super Duty Crew Cab');
insert into modelo (idmarca, nombre) values (6, 'F-250 Super Duty Regular Cab');
insert into modelo (idmarca, nombre) values (6, 'F-250 Super Duty SuperCab');
insert into modelo (idmarca, nombre) values (6, 'F-350 Super Duty Crew Cab');
insert into modelo (idmarca, nombre) values (6, 'F-350 Super Duty Regular Cab');
insert into modelo (idmarca, nombre) values (6, 'F-350 Super Duty SuperCab');
insert into modelo (idmarca, nombre) values (6, 'F-450 Super Duty Crew Cab');
insert into modelo (idmarca, nombre) values (6, 'Fiesta Hatchback');
insert into modelo (idmarca, nombre) values (6, 'Fiesta ST');
insert into modelo (idmarca, nombre) values (6, 'Fiesta Sedan');
insert into modelo (idmarca, nombre) values (6, 'Flex Wagon');
insert into modelo (idmarca, nombre) values (6, 'Focus Electric');
insert into modelo (idmarca, nombre) values (6, 'Focus Hatchback');
insert into modelo (idmarca, nombre) values (6, 'Focus Sedan');
insert into modelo (idmarca, nombre) values (6, 'Focus ST Hatchback');
insert into modelo (idmarca, nombre) values (6, 'Fusion Hybrid');
insert into modelo (idmarca, nombre) values (6, 'Fusion Sedan');
insert into modelo (idmarca, nombre) values (6, 'Fusion Energi Sedan');
insert into modelo (idmarca, nombre) values (6, 'Fusion Hybrid Sedan');
insert into modelo (idmarca, nombre) values (6, 'Mustang Boss 302');
insert into modelo (idmarca, nombre) values (6, 'Mustang Convertible');
insert into modelo (idmarca, nombre) values (6, 'Mustang Coupe');
insert into modelo (idmarca, nombre) values (6, 'Ranger Regular Cab');
insert into modelo (idmarca, nombre) values (6, 'Ranger SuperCab');
insert into modelo (idmarca, nombre) values (6, 'Shelby GT350 Coupe');
insert into modelo (idmarca, nombre) values (6, 'Shelby GT500 Convertible');
insert into modelo (idmarca, nombre) values (6, 'Shelby GT500 Coupe');
insert into modelo (idmarca, nombre) values (6, 'Taurus SHO');
insert into modelo (idmarca, nombre) values (6, 'Taurus Sedan');
insert into modelo (idmarca, nombre) values (6, 'Transit Connect Minivan');


-- honda
insert into modelo (idmarca, nombre) values (7, 'Accord Coupe');
insert into modelo (idmarca, nombre) values (7, 'Accord Sedan');
insert into modelo (idmarca, nombre) values (7, 'Accord Crosstour Hatchback');
insert into modelo (idmarca, nombre) values (7, 'Accord Hybrid Sedan');
insert into modelo (idmarca, nombre) values (7, 'Accord Plug-In Hybrid Sedan');
insert into modelo (idmarca, nombre) values (7, 'Civic Coupe');
insert into modelo (idmarca, nombre) values (7, 'Civic Hybrid');
insert into modelo (idmarca, nombre) values (7, 'Civic Sedan');
insert into modelo (idmarca, nombre) values (7, 'Civic Si');
insert into modelo (idmarca, nombre) values (7, 'Civic Si w/Navigation');
insert into modelo (idmarca, nombre) values (7, 'Civic Si w/Navigation and Summer Tires');
insert into modelo (idmarca, nombre) values (7, 'Civic Si w/Summer Tires');
insert into modelo (idmarca, nombre) values (7, 'Crosstour Hatchback');
insert into modelo (idmarca, nombre) values (7, 'CR-V SUV');
insert into modelo (idmarca, nombre) values (7, 'CR-Z Hatchback');
insert into modelo (idmarca, nombre) values (7, 'Element SUV');
insert into modelo (idmarca, nombre) values (7, 'Fit Hatchback');
insert into modelo (idmarca, nombre) values (7, 'HR-V SUV');
insert into modelo (idmarca, nombre) values (7, 'Insight Hatchback');
insert into modelo (idmarca, nombre) values (7, 'Odyssey Minivan');
insert into modelo (idmarca, nombre) values (7, 'Pilot SUV');
insert into modelo (idmarca, nombre) values (7, 'Ridgeline Crew Cab');


-- Hyundai
insert into modelo (idmarca, nombre) values (8, 'Accent Hatchback');
insert into modelo (idmarca, nombre) values (8, 'Accent Sedan');
insert into modelo (idmarca, nombre) values (8, 'Azera Sedan');
insert into modelo (idmarca, nombre) values (8, 'Elantra Sedan');
insert into modelo (idmarca, nombre) values (8, 'Elantra Coupe');
insert into modelo (idmarca, nombre) values (8, 'Elantra GT Hatchback');
insert into modelo (idmarca, nombre) values (8, 'Elantra Touring Hatchback');
insert into modelo (idmarca, nombre) values (8, 'Equus Sedan');
insert into modelo (idmarca, nombre) values (8, 'Genesis 5.0 R-Spec');
insert into modelo (idmarca, nombre) values (8, 'Genesis Sedan');
insert into modelo (idmarca, nombre) values (8, 'Genesis Coupe');
insert into modelo (idmarca, nombre) values (8, 'Santa Fe SUV');
insert into modelo (idmarca, nombre) values (8, 'Santa Fe Sport SUV');
insert into modelo (idmarca, nombre) values (8, 'Sonata Sedan');
insert into modelo (idmarca, nombre) values (8, 'Sonata Hybrid Sedan');
insert into modelo (idmarca, nombre) values (8, 'Tucson SUV');
insert into modelo (idmarca, nombre) values (8, 'Veloster Hatchback');
insert into modelo (idmarca, nombre) values (8, 'Veracruz SUV');


-- Jaguar
insert into modelo (idmarca, nombre) values (9, 'F-TYPE Convertible');
insert into modelo (idmarca, nombre) values (9, 'F-TYPE Coupe');
insert into modelo (idmarca, nombre) values (9, 'F-TYPE Project 7');
insert into modelo (idmarca, nombre) values (9, 'F-TYPE R');
insert into modelo (idmarca, nombre) values (9, 'XF Sedan');
insert into modelo (idmarca, nombre) values (9, 'XF XFR');
insert into modelo (idmarca, nombre) values (9, 'XF XFR-S');
insert into modelo (idmarca, nombre) values (9, 'XJ Sedan');
insert into modelo (idmarca, nombre) values (9, 'XJ Supersport');
insert into modelo (idmarca, nombre) values (9, 'XJ XJL Supersport');
insert into modelo (idmarca, nombre) values (9, 'XJ XJL Ultimate');
insert into modelo (idmarca, nombre) values (9, 'XJ XJR');
insert into modelo (idmarca, nombre) values (9, 'XJ XJR LWB');
insert into modelo (idmarca, nombre) values (9, 'XK Convertible');
insert into modelo (idmarca, nombre) values (9, 'XK Coupe');
insert into modelo (idmarca, nombre) values (9, 'XK XKR');
insert into modelo (idmarca, nombre) values (9, 'XK XKR-S');
insert into modelo (idmarca, nombre) values (9, 'XK XKR-S GT');
insert into modelo (idmarca, nombre) values (9, 'XK XKR175 75th Anniversary Limited Ed.');


-- Kia
insert into modelo (idmarca, nombre) values (10, 'Cadenza Sedan');
insert into modelo (idmarca, nombre) values (10, 'Forte Coupe');
insert into modelo (idmarca, nombre) values (10, 'Forte Hatchback');
insert into modelo (idmarca, nombre) values (10, 'Forte Koup');
insert into modelo (idmarca, nombre) values (10, 'Forte Sedan');
insert into modelo (idmarca, nombre) values (10, 'K900 Sedan');
insert into modelo (idmarca, nombre) values (10, 'Optima Hybrid');
insert into modelo (idmarca, nombre) values (10, 'Optima Sedan');
insert into modelo (idmarca, nombre) values (10, 'Optima Hybrid Sedan');
insert into modelo (idmarca, nombre) values (10, 'Rio Hatchback');
insert into modelo (idmarca, nombre) values (10, 'Rio Sedan');
insert into modelo (idmarca, nombre) values (10, 'Sedona Minivan');
insert into modelo (idmarca, nombre) values (10, 'Sorento SUV');
insert into modelo (idmarca, nombre) values (10, 'Soul Wagon');
insert into modelo (idmarca, nombre) values (10, 'Sportage SUV');


-- Land Rover
insert into modelo (idmarca, nombre) values (11, 'Discovery Sport SUV');
insert into modelo (idmarca, nombre) values (11, 'LR2 SUV');
insert into modelo (idmarca, nombre) values (11, 'LR4 SUV');
insert into modelo (idmarca, nombre) values (11, 'Range Rover SUV');
insert into modelo (idmarca, nombre) values (11, 'Range Rover SV Autobiography LWB');
insert into modelo (idmarca, nombre) values (11, 'Range Rover Evoque SUV');
insert into modelo (idmarca, nombre) values (11, 'Range Rover Sport SUV');
insert into modelo (idmarca, nombre) values (11, 'Range Rover Sport SVR');


-- Mazda
insert into modelo (idmarca, nombre) values (12, '2 Hatchback');
insert into modelo (idmarca, nombre) values (12, '3 Hatchback');
insert into modelo (idmarca, nombre) values (12, '3 Sedan');
insert into modelo (idmarca, nombre) values (12, '5 Minivan');
insert into modelo (idmarca, nombre) values (12, '6 Sedan');
insert into modelo (idmarca, nombre) values (12, 'CX-3 SUV');
insert into modelo (idmarca, nombre) values (12, 'CX-5 SUV');
insert into modelo (idmarca, nombre) values (12, 'CX-7 SUV');
insert into modelo (idmarca, nombre) values (12, 'CX-9 SUV');
insert into modelo (idmarca, nombre) values (12, 'Mazdaspeed 3 Hatchback');
insert into modelo (idmarca, nombre) values (12, 'MX-5 Miata Convertible');
insert into modelo (idmarca, nombre) values (12, 'RX-8 Coupe');
insert into modelo (idmarca, nombre) values (12, 'Tribute SUV');


-- Mercedes-Benz
insert into modelo (idmarca, nombre) values (13, 'C-Class AMG C 43');
insert into modelo (idmarca, nombre) values (13, 'C-Class AMG C 63');
insert into modelo (idmarca, nombre) values (13, 'C-Class AMG C 63 S');
insert into modelo (idmarca, nombre) values (13, 'C-Class C 450 AMG 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'C-Class C63 AMG');
insert into modelo (idmarca, nombre) values (13, 'C-Class Coupe');
insert into modelo (idmarca, nombre) values (13, 'C-Class Sedan');
insert into modelo (idmarca, nombre) values (13, 'CLA-Class AMG CLA 45');
insert into modelo (idmarca, nombre) values (13, 'CLA-Class CLA 45 AMG');
insert into modelo (idmarca, nombre) values (13, 'CLA-Class Sedan');
insert into modelo (idmarca, nombre) values (13, 'CL-Class CL63 AMG');
insert into modelo (idmarca, nombre) values (13, 'CL-Class Coupe');
insert into modelo (idmarca, nombre) values (13, 'CLS-Class AMG CLS 63 S');
insert into modelo (idmarca, nombre) values (13, 'CLS-Class AMG CLS 63 S 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'CLS-Class CLS 63 AMG 4MATIC S-Model');
insert into modelo (idmarca, nombre) values (13, 'CLS-Class CLS63 AMG');
insert into modelo (idmarca, nombre) values (13, 'CLS-Class CLS63 AMG 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'CLS-Class Sedan');
insert into modelo (idmarca, nombre) values (13, 'E-Class AMG E 43 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'E-Class AMG E 63 4MATIC S-Model');
insert into modelo (idmarca, nombre) values (13, 'E-Class Convertible');
insert into modelo (idmarca, nombre) values (13, 'E-Class Coupe');
insert into modelo (idmarca, nombre) values (13, 'E-Class Diesel');
insert into modelo (idmarca, nombre) values (13, 'E-Class E 63 AMG 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'E-Class E 63 AMG 4MATIC S-Model');
insert into modelo (idmarca, nombre) values (13, 'E-Class E63 AMG');
insert into modelo (idmarca, nombre) values (13, 'E-Class Hybrid');
insert into modelo (idmarca, nombre) values (13, 'E-Class Sedan');
insert into modelo (idmarca, nombre) values (13, 'E-Class Wagon');
insert into modelo (idmarca, nombre) values (13, 'G-Class AMG G 63');
insert into modelo (idmarca, nombre) values (13, 'G-Class G 63 AMG');
insert into modelo (idmarca, nombre) values (13, 'G-Class G55 AMG');
insert into modelo (idmarca, nombre) values (13, 'G-Class SUV');
insert into modelo (idmarca, nombre) values (13, 'GLA-Class AMG GLA 45 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'GLA-Class GLA 45 AMG 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'GLA-Class SUV');
insert into modelo (idmarca, nombre) values (13, 'GLC-Class SUV');
insert into modelo (idmarca, nombre) values (13, 'GL-Class Diesel');
insert into modelo (idmarca, nombre) values (13, 'GL-Class GL 63 AMG');
insert into modelo (idmarca, nombre) values (13, 'GL-Class SUV');
insert into modelo (idmarca, nombre) values (13, 'GLE-Class AMG GLE 63 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'GLE-Class AMG GLE 63 S 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'GLE-Class SUV');
insert into modelo (idmarca, nombre) values (13, 'GLK-Class Diesel');
insert into modelo (idmarca, nombre) values (13, 'GLK-Class SUV');
insert into modelo (idmarca, nombre) values (13, 'M-Class Diesel');
insert into modelo (idmarca, nombre) values (13, 'M-Class Hybrid');
insert into modelo (idmarca, nombre) values (13, 'M-Class ML 63 AMG');
insert into modelo (idmarca, nombre) values (13, 'M-Class SUV');
insert into modelo (idmarca, nombre) values (13, 'R-Class Diesel');
insert into modelo (idmarca, nombre) values (13, 'R-Class Wagon');
insert into modelo (idmarca, nombre) values (13, 'S-Class AMG S 63');
insert into modelo (idmarca, nombre) values (13, 'S-Class AMG S 63 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'S-Class AMG S 65');
insert into modelo (idmarca, nombre) values (13, 'S-Class Coupe');
insert into modelo (idmarca, nombre) values (13, 'S-Class Diesel');
insert into modelo (idmarca, nombre) values (13, 'S-Class Hybrid');
insert into modelo (idmarca, nombre) values (13, 'S-Class S 63 AMG 4MATIC');
insert into modelo (idmarca, nombre) values (13, 'S-Class S 65 AMG');
insert into modelo (idmarca, nombre) values (13, 'S-Class S63 AMG');
insert into modelo (idmarca, nombre) values (13, 'S-Class Sedan');
insert into modelo (idmarca, nombre) values (13, 'SL-Class Convertible');
insert into modelo (idmarca, nombre) values (13, 'SL-Class SL 63 AMG');
insert into modelo (idmarca, nombre) values (13, 'SL-Class SL 65 AMG');
insert into modelo (idmarca, nombre) values (13, 'SLK-Class AMG SLK 55');
insert into modelo (idmarca, nombre) values (13, 'SLK-Class Convertible');
insert into modelo (idmarca, nombre) values (13, 'SLK-Class SLK 55 AMG');
insert into modelo (idmarca, nombre) values (13, 'SLS AMG Coupe');
insert into modelo (idmarca, nombre) values (13, 'Sprinter Van');


-- Mercury
insert into modelo (idmarca, nombre) values (14, 'Grand Marquis Sedan');
insert into modelo (idmarca, nombre) values (14, 'Mariner SUV');
insert into modelo (idmarca, nombre) values (14, 'Mariner Hybrid SUV');
insert into modelo (idmarca, nombre) values (14, 'Milan Sedan');
insert into modelo (idmarca, nombre) values (14, 'Milan Hybrid Sedan');


-- Mitsubishi
insert into modelo (idmarca, nombre) values (15, 'Eclipse Hatchback');
insert into modelo (idmarca, nombre) values (15, 'Eclipse Spyder Convertible');
insert into modelo (idmarca, nombre) values (15, 'Endeavor SUV');
insert into modelo (idmarca, nombre) values (15, 'Galant Sedan');
insert into modelo (idmarca, nombre) values (15, 'Lancer Sedan');
insert into modelo (idmarca, nombre) values (15, 'Lancer Evolution Sedan');
insert into modelo (idmarca, nombre) values (15, 'Lancer Sportback Hatchback');
insert into modelo (idmarca, nombre) values (15, 'Mirage Hatchback');
insert into modelo (idmarca, nombre) values (15, 'Outlander SUV');
insert into modelo (idmarca, nombre) values (15, 'Outlander Sport SUV');


-- Nissan
insert into modelo (idmarca, nombre) values (16, '370Z Convertible');
insert into modelo (idmarca, nombre) values (16, '370Z Coupe');
insert into modelo (idmarca, nombre) values (16, '370Z NISMO');
insert into modelo (idmarca, nombre) values (16, '370Z NISMO Tech');
insert into modelo (idmarca, nombre) values (16, 'Altima Coupe');
insert into modelo (idmarca, nombre) values (16, 'Altima Sedan');
insert into modelo (idmarca, nombre) values (16, 'Altima Hybrid Sedan');
insert into modelo (idmarca, nombre) values (16, 'Armada SUV');
insert into modelo (idmarca, nombre) values (16, 'Cube Wagon');
insert into modelo (idmarca, nombre) values (16, 'Frontier Crew Cab');
insert into modelo (idmarca, nombre) values (16, 'Frontier King Cab');
insert into modelo (idmarca, nombre) values (16, 'GT-R Coupe');
insert into modelo (idmarca, nombre) values (16, 'GT-R NISMO');
insert into modelo (idmarca, nombre) values (16, 'Juke Hatchback');
insert into modelo (idmarca, nombre) values (16, 'Juke NISMO');
insert into modelo (idmarca, nombre) values (16, 'Juke NISMO RS');
insert into modelo (idmarca, nombre) values (16, 'Leaf Hatchback');
insert into modelo (idmarca, nombre) values (16, 'Maxima Sedan');
insert into modelo (idmarca, nombre) values (16, 'Murano SUV');
insert into modelo (idmarca, nombre) values (16, 'Murano CrossCabriolet SUV');
insert into modelo (idmarca, nombre) values (16, 'NV200 Minivan');
insert into modelo (idmarca, nombre) values (16, 'NV Van');
insert into modelo (idmarca, nombre) values (16, 'NV Passenger Van');
insert into modelo (idmarca, nombre) values (16, 'Pathfinder Hybrid');
insert into modelo (idmarca, nombre) values (16, 'Pathfinder SUV');
insert into modelo (idmarca, nombre) values (16, 'Quest Minivan');
insert into modelo (idmarca, nombre) values (16, 'Rogue SUV');
insert into modelo (idmarca, nombre) values (16, 'Rogue Select SUV');
insert into modelo (idmarca, nombre) values (16, 'Sentra SE-R Spec V');
insert into modelo (idmarca, nombre) values (16, 'Sentra Sedan');
insert into modelo (idmarca, nombre) values (16, 'Titan Crew Cab');
insert into modelo (idmarca, nombre) values (16, 'Titan King Cab');
insert into modelo (idmarca, nombre) values (16, 'Titan XD Crew Cab');
insert into modelo (idmarca, nombre) values (16, 'Versa Hatchback');
insert into modelo (idmarca, nombre) values (16, 'Versa Sedan');
insert into modelo (idmarca, nombre) values (16, 'Versa Note Hatchback');
insert into modelo (idmarca, nombre) values (16, 'Xterra SUV');


-- Porsche
insert into modelo (idmarca, nombre) values (17, '911 Carrera 4 GTS');
insert into modelo (idmarca, nombre) values (17, '911 Carrera GTS');
insert into modelo (idmarca, nombre) values (17, '911 Convertible');
insert into modelo (idmarca, nombre) values (17, '911 Coupe');
insert into modelo (idmarca, nombre) values (17, '911 GT2 RS');
insert into modelo (idmarca, nombre) values (17, '911 GT3');
insert into modelo (idmarca, nombre) values (17, '911 GT3 RS');
insert into modelo (idmarca, nombre) values (17, '911 GT3 RS 4.0');
insert into modelo (idmarca, nombre) values (17, 'Boxster Convertible');
insert into modelo (idmarca, nombre) values (17, 'Cayenne Hybrid');
insert into modelo (idmarca, nombre) values (17, 'Cayenne SUV');
insert into modelo (idmarca, nombre) values (17, 'Cayman Coupe');
insert into modelo (idmarca, nombre) values (17, 'Panamera Hybrid');
insert into modelo (idmarca, nombre) values (17, 'Panamera Sedan');


-- Subaru
insert into modelo (idmarca, nombre) values (18, 'BRZ Coupe');
insert into modelo (idmarca, nombre) values (18, 'Crosstrek SUV');
insert into modelo (idmarca, nombre) values (18, 'Forester SUV');
insert into modelo (idmarca, nombre) values (18, 'Impreza Hatchback');
insert into modelo (idmarca, nombre) values (18, 'Impreza Sedan');
insert into modelo (idmarca, nombre) values (18, 'Impreza WRX STI');
insert into modelo (idmarca, nombre) values (18, 'Impreza WRX STI Limited');
insert into modelo (idmarca, nombre) values (18, 'Impreza WRX Hatchback');
insert into modelo (idmarca, nombre) values (18, 'Impreza WRX STI');
insert into modelo (idmarca, nombre) values (18, 'Impreza WRX STI Limited');
insert into modelo (idmarca, nombre) values (18, 'Impreza WRX Sedan');
insert into modelo (idmarca, nombre) values (18, 'Legacy Sedan');
insert into modelo (idmarca, nombre) values (18, 'Outback SUV');
insert into modelo (idmarca, nombre) values (18, 'Tribeca SUV');
insert into modelo (idmarca, nombre) values (18, 'WRX STI');
insert into modelo (idmarca, nombre) values (18, 'WRX STI Launch Edition');
insert into modelo (idmarca, nombre) values (18, 'WRX STI Limited');
insert into modelo (idmarca, nombre) values (18, 'WRX STI Series.HyperBlue');
insert into modelo (idmarca, nombre) values (18, 'WRX Sedan');
insert into modelo (idmarca, nombre) values (18, 'XV Crosstrek Hybrid');
insert into modelo (idmarca, nombre) values (18, 'XV Crosstrek SUV');


-- Suzuki
insert into modelo (idmarca, nombre) values (19, 'Equator Crew Cab');
insert into modelo (idmarca, nombre) values (19, 'Equator Extended Cab');
insert into modelo (idmarca, nombre) values (19, 'Grand Vitara SUV');
insert into modelo (idmarca, nombre) values (19, 'Kizashi Sedan');
insert into modelo (idmarca, nombre) values (19, 'SX4 Hatchback');
insert into modelo (idmarca, nombre) values (19, 'SX4 Sedan');


-- Toyota
insert into modelo (idmarca, nombre) values (20, '4Runner SUV');
insert into modelo (idmarca, nombre) values (20, '86 Coupe');
insert into modelo (idmarca, nombre) values (20, 'Avalon Sedan');
insert into modelo (idmarca, nombre) values (20, 'Avalon Hybrid Sedan');
insert into modelo (idmarca, nombre) values (20, 'Camry Sedan');
insert into modelo (idmarca, nombre) values (20, 'Camry Hybrid Sedan');
insert into modelo (idmarca, nombre) values (20, 'Corolla Sedan');
insert into modelo (idmarca, nombre) values (20, 'Corolla iM Hatchback');
insert into modelo (idmarca, nombre) values (20, 'FJ Cruiser SUV');
insert into modelo (idmarca, nombre) values (20, 'Highlander SUV');
insert into modelo (idmarca, nombre) values (20, 'Highlander Hybrid SUV');
insert into modelo (idmarca, nombre) values (20, 'Land Cruiser SUV');
insert into modelo (idmarca, nombre) values (20, 'Matrix Hatchback');
insert into modelo (idmarca, nombre) values (20, 'Prius Hatchback');
insert into modelo (idmarca, nombre) values (20, 'Prius c Hatchback');
insert into modelo (idmarca, nombre) values (20, 'Prius Plug-in Hatchback');
insert into modelo (idmarca, nombre) values (20, 'Prius Prime Hatchback');
insert into modelo (idmarca, nombre) values (20, 'Prius v Wagon');
insert into modelo (idmarca, nombre) values (20, 'RAV4 SUV');
insert into modelo (idmarca, nombre) values (20, 'RAV4 Hybrid SUV');
insert into modelo (idmarca, nombre) values (20, 'Sequoia SUV');
insert into modelo (idmarca, nombre) values (20, 'Sienna Minivan');
insert into modelo (idmarca, nombre) values (20, 'Tacoma Access Cab');
insert into modelo (idmarca, nombre) values (20, 'Tacoma Double Cab');
insert into modelo (idmarca, nombre) values (20, 'Tacoma Regular Cab');
insert into modelo (idmarca, nombre) values (20, 'Tundra CrewMax Cab');
insert into modelo (idmarca, nombre) values (20, 'Tundra Double Cab');
insert into modelo (idmarca, nombre) values (20, 'Tundra Regular Cab');
insert into modelo (idmarca, nombre) values (20, 'Venza Wagon');
insert into modelo (idmarca, nombre) values (20, 'Yaris Hatchback');
insert into modelo (idmarca, nombre) values (20, 'Yaris Sedan');
insert into modelo (idmarca, nombre) values (20, 'Yaris iA Sedan');


-- Volkswagen
insert into modelo (idmarca, nombre) values (21, 'Beetle Diesel');
insert into modelo (idmarca, nombre) values (21, 'Beetle Hatchback');
insert into modelo (idmarca, nombre) values (21, 'Beetle Convertible');
insert into modelo (idmarca, nombre) values (21, 'Beetle Convertible Diesel');
insert into modelo (idmarca, nombre) values (21, 'CC Sedan');
insert into modelo (idmarca, nombre) values (21, 'Eos Convertible');
insert into modelo (idmarca, nombre) values (21, 'GLI Sedan');
insert into modelo (idmarca, nombre) values (21, 'Golf Diesel');
insert into modelo (idmarca, nombre) values (21, 'Golf Hatchback');
insert into modelo (idmarca, nombre) values (21, 'Golf GTI Hatchback');
insert into modelo (idmarca, nombre) values (21, 'Golf R Hatchback');
insert into modelo (idmarca, nombre) values (21, 'Golf SportWagen Wagon');
insert into modelo (idmarca, nombre) values (21, 'GTI Hatchback');
insert into modelo (idmarca, nombre) values (21, 'Jetta 2.0T GLI');
insert into modelo (idmarca, nombre) values (21, 'Jetta 2.0T GLI SE');
insert into modelo (idmarca, nombre) values (21, 'Jetta 2.0T GLI SE PZEV');
insert into modelo (idmarca, nombre) values (21, 'Jetta 2.0T GLI SEL');
insert into modelo (idmarca, nombre) values (21, 'Jetta 2.0T GLI SEL PZEV');
insert into modelo (idmarca, nombre) values (21, 'Jetta Diesel');
insert into modelo (idmarca, nombre) values (21, 'Jetta GLI SE');
insert into modelo (idmarca, nombre) values (21, 'Jetta GLI SE PZEV');
insert into modelo (idmarca, nombre) values (21, 'Jetta GLI SEL');
insert into modelo (idmarca, nombre) values (21, 'Jetta GLI SEL PZEV');
insert into modelo (idmarca, nombre) values (21, 'Jetta Hybrid');
insert into modelo (idmarca, nombre) values (21, 'Jetta Sedan');
insert into modelo (idmarca, nombre) values (21, 'Jetta GLI Sedan');
insert into modelo (idmarca, nombre) values (21, 'Jetta Hybrid Sedan');
insert into modelo (idmarca, nombre) values (21, 'Jetta SportWagen Diesel');
insert into modelo (idmarca, nombre) values (21, 'Jetta SportWagen Wagon');
insert into modelo (idmarca, nombre) values (21, 'Passat Diesel');
insert into modelo (idmarca, nombre) values (21, 'Passat Sedan');
insert into modelo (idmarca, nombre) values (21, 'Routan Minivan');
insert into modelo (idmarca, nombre) values (21, 'Tiguan SUV');
insert into modelo (idmarca, nombre) values (21, 'Touareg Diesel');
insert into modelo (idmarca, nombre) values (21, 'Touareg Hybrid');
insert into modelo (idmarca, nombre) values (21, 'Touareg SUV');

-- Volvo
insert into modelo (idmarca, nombre) values (22, 'C30 Hatchback');
insert into modelo (idmarca, nombre) values (22, 'C70 Convertible');
insert into modelo (idmarca, nombre) values (22, 'S40 Sedan');
insert into modelo (idmarca, nombre) values (22, 'S60 Sedan');
insert into modelo (idmarca, nombre) values (22, 'S60 T6 Polestar');
insert into modelo (idmarca, nombre) values (22, 'S60 T6 R-Design');
insert into modelo (idmarca, nombre) values (22, 'S60 Cross Country Sedan');
insert into modelo (idmarca, nombre) values (22, 'S80 Sedan');
insert into modelo (idmarca, nombre) values (22, 'V50 Wagon');
insert into modelo (idmarca, nombre) values (22, 'V60 Wagon');
insert into modelo (idmarca, nombre) values (22, 'V60 Cross Country Wagon');
insert into modelo (idmarca, nombre) values (22, 'XC60 SUV');
insert into modelo (idmarca, nombre) values (22, 'XC70 Wagon');
insert into modelo (idmarca, nombre) values (22, 'XC90 SUV');

