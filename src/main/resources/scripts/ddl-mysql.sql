drop database if exists car_rental;

create database car_rental;

use car_rental;

create table usuario (
    id int primary key auto_increment,
    nombre varchar(50),
    apellido varchar(50),
    email varchar(100) not null,
    fechanacimiento date,
    dni varchar(8),
    contrasena varchar(50),
    unique key(dni),
    unique key(email)
);

create table marca (
    id int primary key auto_increment,
    nombre varchar(50)
);

create table modelo (
    id int primary key auto_increment,
    nombre varchar(50),
    idmarca integer not null,
    foreign key (idmarca) references marca(id)
);

create table carro (
    id int primary key auto_increment,
    idmodelo integer not null,
    anio integer not null,
    detalle varchar(1000) not null,
    pasajeros integer,
    idusuario integer not null,
    preciodiario numeric(7, 2) NOT NULL,
    disponible boolean not null default true,
    foreign key (idmodelo) references modelo(id),
    foreign key(idusuario) references usuario(id)
);

create table imagen_carro (
    id integer primary key auto_increment,
    idcarro integer NOT NULL,
    imagen LONGBLOB NOT NULL,
    nombre varchar(100) NOT NULL,
    foreign key(idcarro) references carro(id)
);

create table reserva_carro(
    id int primary key auto_increment,
    idcarro int not null,
    fechainicio date not null,
    fechafin date not null,
    idusuario int not null,
    puntaje int default 0,
    comentario varchar(1000),
    preciodiario decimal(8,2) not null,
    preciototal decimal(8,2) not null,
    estado varchar(15) not null default 'PENDIENTE',
    pagado boolean not null default false,
    foreign key (idcarro) references carro(id),
    foreign key (idusuario) references usuario(id)
);
