create table usuario (
    id serial primary key,
    nombre varchar(50),
    apellido varchar(50),
    email varchar(100) not null,
    fechanacimiento date,
    dni varchar(8),
    contrasena varchar(50),
    CONSTRAINT usuario_dni_key UNIQUE (dni),
    CONSTRAINT usuario_email_key UNIQUE (email)
);

CREATE UNIQUE INDEX idx_usuario_email ON usuario (email);

create table marca (
    id serial primary key,
    nombre varchar(50)
);

create table modelo (
    id serial primary key,
    nombre varchar(50),
    idmarca integer not null references marca(id)
);

create index idx_modelo_marca on modelo(idmarca);

create table carro (
    id serial primary key,
    idmodelo integer not null references modelo(id),
    anio integer not null,
    detalle varchar(1000) not null,
    pasajeros integer,
    idusuario integer not null references usuario(id),
    preciodiario numeric(7, 2) NOT NULL,
    disponible boolean not null default true
);

create index idx_carro_usuario on carro(idusuario);

/*
create table disponibilidadcarro (
    id serial primary key,
    idcarro int not null references carro(id),
    fechainicio date not null,
    fechafin date not null
);
*/

CREATE TABLE imagen_carro(
    id serial primary key,
    idcarro integer NOT NULL references carro(id),
    imagen bytea NOT NULL,
    nombre character varying(100) COLLATE pg_catalog."default" NOT NULL
);

create table reserva_carro(
    id serial primary key,
    idcarro int not null references carro(id),
    fechainicio date not null,
    fechafin date not null,
    idusuario int not null references usuario(id),
    puntaje int,
    comentario varchar(1000),
    preciodiario decimal(8,2) not null,
    preciototal decimal(8,2) not null,
    estado varchar(15) not null default 'PENDIENTE'
);
